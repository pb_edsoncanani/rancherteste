package br.com.portobello.occintegrationout.infra.repository.schemas;

public enum Status {
    SUCCESS, PROCESSING, ERROR
}
