package br.com.portobello.occintegrationout.infra.repository;

import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegrationControlRepository extends MongoRepository<IntegrationControl, String>, IntegrationControlRepositoryCustom {

}
