package br.com.portobello.occintegrationout.infra.repository;

import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface IntegrationControlRepositoryCustom {

    Optional<IntegrationControl> findLastIntegrationControl(@NotNull IntegrationType integrationType, @NotNull Status status);
    Optional<IntegrationControl> findLastIntegrationControlWithStatusSuccess(@NotNull IntegrationType integrationType);
}
