package br.com.portobello.occintegrationout.infra.repository;

import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public class IntegrationControlRepositoryCustomImpl implements IntegrationControlRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public IntegrationControlRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Optional<IntegrationControl> findLastIntegrationControl(@NotNull IntegrationType integrationType, @NotNull Status status) {
        Query query = new Query();

        query.addCriteria(Criteria.where("integrationType").is(integrationType));
        query.addCriteria(Criteria.where("status").is(status));

        query.with(Sort.by(Sort.Direction.DESC, "startDate"));
        query.limit(1);

        return mongoTemplate.find(query, IntegrationControl.class).stream().findFirst();
                /*.stream().findFirst().orElseGet(() -> {
            throw new NotFoundException("Nenhum registro encontrado para busca: integrationType {} e status {}.", integrationType, status);
        });*/
    }

    @Override
    public Optional<IntegrationControl> findLastIntegrationControlWithStatusSuccess(@NotNull IntegrationType integrationType) {
        return findLastIntegrationControl(integrationType, Status.SUCCESS);
    }
}
