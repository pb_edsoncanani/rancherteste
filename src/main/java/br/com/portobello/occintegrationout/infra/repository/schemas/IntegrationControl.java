package br.com.portobello.occintegrationout.infra.repository.schemas;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Document(collection = "integration-control")
public class IntegrationControl {

    @Id
    private String id;

    @NotNull
    private Status status;

    @NotNull
    private IntegrationType integrationType;

    @NotNull
    private Instant startDate = Instant.now();

    private Instant endDate;

    private Integer totalRecords;

    private Integer totalRecordsSuccessfully;

    private Integer totalErrorRecords;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public IntegrationType getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(IntegrationType integrationType) {
        this.integrationType = integrationType;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalRecordsSuccessfully() {
        return totalRecordsSuccessfully;
    }

    public void setTotalRecordsSuccessfully(Integer totalRecordsSuccessfully) {
        this.totalRecordsSuccessfully = totalRecordsSuccessfully;
    }

    public Integer getTotalErrorRecords() {
        return totalErrorRecords;
    }

    public void setTotalErrorRecords(Integer totalErrorRecords) {
        this.totalErrorRecords = totalErrorRecords;
    }

    @Override
    public String toString() {
        return "IntegrationControl{" +
                "id='" + id + '\'' +
                ", status=" + status +
                ", integrationType=" + integrationType +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", totalRecords=" + totalRecords +
                ", totalRecordsSuccessfully=" + totalRecordsSuccessfully +
                ", totalErrorRecords=" + totalErrorRecords +
                '}';
    }
}
