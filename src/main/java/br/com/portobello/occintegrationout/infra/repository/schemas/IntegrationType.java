package br.com.portobello.occintegrationout.infra.repository.schemas;

public enum IntegrationType {
    CATALOG, COLLECTION, PRICE_LIST_GROUP, PRODUCTS, SKU, PROFILE, PRICE, ORGANIZATIONS, ADMIN_PROFILE;
}
