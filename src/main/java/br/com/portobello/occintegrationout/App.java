package br.com.portobello.occintegrationout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = {"br.com.portobello"})
@EnableAsync()
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}
