package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccPriceListGroupDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("locale")
    private String locale;

    @JsonProperty("displayName")
    private String displayName;

    @JsonProperty("includeAllProducts")
    private boolean includeAllProducts = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isIncludeAllProducts() {
        return includeAllProducts;
    }

    public void setIncludeAllProducts(boolean includeAllProducts) {
        this.includeAllProducts = includeAllProducts;
    }

    @Override
    public String toString() {
        return "OccPriceListGroupDto{" +
                "id='" + id + '\'' +
                ", locale='" + locale + '\'' +
                ", displayName='" + displayName + '\'' +
                ", includeAllProducts=" + includeAllProducts +
                '}';
    }
}
