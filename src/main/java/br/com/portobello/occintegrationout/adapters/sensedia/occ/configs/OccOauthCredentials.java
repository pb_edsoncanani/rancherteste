package br.com.portobello.occintegrationout.adapters.sensedia.occ.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OccOauthCredentials {

    @Value("${app.occ.oauth2.url}")
    private String occLoginURL;

    @Value("${app.occ.oauth2.jwt}")
    private String occJwtLogin;

    @Value("${app.occ.oauth2.url}")
    private String occRefreshTokenURL;

    public String getOccLoginURL() {
        return occLoginURL;
    }

    public String getOccJwtLogin() {
        return occJwtLogin;
    }

    public String getOccRefreshTokenURL() {
        return occRefreshTokenURL;
    }
}
