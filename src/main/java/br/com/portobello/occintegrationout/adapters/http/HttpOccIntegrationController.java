package br.com.portobello.occintegrationout.adapters.http;

import br.com.portobello.occintegrationout.application.AsyncProcessApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class HttpOccIntegrationController {

    private Logger logger = LoggerFactory.getLogger(HttpOccIntegrationController.class);

    @Autowired
    private AsyncProcessApplication asyncProcessApplication;

    @PostMapping("/sync-catalog")
    public ResponseEntity syncCatalog() {
        logger.info("starting POST - [/sync-catalog]");
        asyncProcessApplication.startCatalogSync();
        logger.info("finishing POST - [/sync-catalog]");

        return ResponseEntity.ok().build();
    }

    @PostMapping("/sync-price-list-group")
    public ResponseEntity syncPriceListGroup() {
        logger.info("starting POST - [/sync-price-list-group]");
        asyncProcessApplication.startPriceListGroupSync();
        logger.info("finishing POST - [/sync-price-list-group]");

        return ResponseEntity.ok().build();
    }

    @PostMapping("/sync-products")
    public ResponseEntity syncProducts() {
        logger.info("starting POST - [/sync-products]");
        asyncProcessApplication.startProductSync();
        logger.info("finishing POST - [/sync-products]");

        return ResponseEntity.ok().build();
    }

    @PostMapping("/sync-skus")
    public ResponseEntity syncSkus() {
        logger.info("starting POST - [/sync-skus]");
        asyncProcessApplication.startSkuSync();
        logger.info("finishing POST - [/sync-skus]");

        return ResponseEntity.ok().build();
    }

    @PostMapping("/sync-prices")
    public ResponseEntity syncPrices() {
        logger.info("starting POST - [/sync-prices]");
        asyncProcessApplication.startPriceSync();
        logger.info("finishing POST - [/sync-prices]");

        return ResponseEntity.ok().build();
    }

    @PostMapping("/sync-users")
    public ResponseEntity startUserFlow() {
        logger.info("starting POST - [/sync-users]");
        asyncProcessApplication.startUserFlow();
        logger.info("finishing POST - [/sync-users]");

        return ResponseEntity.ok().build();
    }
}
