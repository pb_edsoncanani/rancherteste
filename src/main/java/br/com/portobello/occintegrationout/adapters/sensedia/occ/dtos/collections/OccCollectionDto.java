package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.collections;

import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.Properties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccCollectionDto {

    private String catalogId;
    private Properties properties = new Properties();

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "OccCollectionDto{" +
                "catalogId='" + catalogId + '\'' +
                ", properties=" + properties +
                '}';
    }
}
