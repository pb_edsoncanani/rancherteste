package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccOrganizationsDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("billingAddress")
    private OccBillingAddressDto billingAddress;

    @JsonProperty("members")
    private List<String> members = new ArrayList<>();

    @JsonProperty("parentOrganization")
    private OccParentOrganizationDto parentOrganization;

    @JsonProperty("x_id_ebs")
    private String xIdEbs;

    @JsonProperty("dynamicPropertyNotAdminIds")
    private String dynamicPropertyNotAdminIds;

    @JsonProperty("op")
    private String op;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public OccParentOrganizationDto getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(OccParentOrganizationDto parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public String getDynamicPropertyNotAdminIds() {
        return dynamicPropertyNotAdminIds;
    }

    public void setDynamicPropertyNotAdminIds(String dynamicPropertyNotAdminIds) {
        this.dynamicPropertyNotAdminIds = dynamicPropertyNotAdminIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getxIdEbs() {
        return xIdEbs;
    }

    public void setxIdEbs(String xIdEbs) {
        this.xIdEbs = xIdEbs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OccBillingAddressDto getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(OccBillingAddressDto billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public void addMember(String member){
        this.members.add(member);
    }

    public void removeMember(String member){
        this.members.remove(member);
    }

    @Override
    public String toString() {
        return "OccOrganizationsDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", billingAddress=" + billingAddress +
                ", members=" + members +
                ", parentOrganization=" + parentOrganization +
                ", xIdEbs='" + xIdEbs + '\'' +
                ", dynamicPropertyNotAdminIds='" + dynamicPropertyNotAdminIds + '\'' +
                '}';
    }
}
