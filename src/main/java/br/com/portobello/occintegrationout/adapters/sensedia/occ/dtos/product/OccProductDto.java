package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccProductDto {

    @JsonProperty("properties")
    private OccProductPropertiesDto properties;

    public OccProductPropertiesDto getProperties() {
        return properties;
    }

    public void setProperties(OccProductPropertiesDto properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "OccProductDto{" +
                "properties=" + properties +
                '}';
    }
}
