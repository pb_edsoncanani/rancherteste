package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccPriceListActivationDto {

    @JsonProperty("priceListGroupId")
    private String priceListGroupId;

    public OccPriceListActivationDto(String priceListGroupId) {
        this.priceListGroupId = priceListGroupId;
    }

    public String getPriceListGroupId() {
        return priceListGroupId;
    }

    @Override
    public String toString() {
        return "OccPriceListActivationDto{" +
                "priceListGroupId='" + priceListGroupId + '\'' +
                '}';
    }
}
