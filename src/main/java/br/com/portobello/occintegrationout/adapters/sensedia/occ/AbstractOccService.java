package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccErrorResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

public abstract class AbstractOccService<T> implements OccService<T> {

    public static final String HEADER_X_CCASSET_LANGUAGE = "X-CCAsset-Language";
    protected final RestTemplate restTemplate;
    protected final SensediaAuthentication authentication;
    @Value("${app.occ.headers.xCCAssetLanguage}")
    protected String xOccAssetLanguage;
    private Logger logger = LoggerFactory.getLogger(AbstractOccService.class);

    public AbstractOccService(SensediaAuthentication authentication,
                              RestTemplateBuilder builder) {
        this.authentication = authentication;
        this.restTemplate = builder.build();
    }

    protected boolean isOccNotFoundCode(String occResponseString) {
        // 20031 code in OCC is equivalent as (invalid or non-existent)
        try {
            ObjectMapper mapper = new ObjectMapper();
            OccErrorResponse response = mapper.readValue(occResponseString, OccErrorResponse.class);
            if (response.getErrorCode().equals(getOccNotFoundCode())) {
                return true;
            }
        } catch (IOException ex) {
            this.logger.error("Error in transform HttpResponseBody to OccErrorResponse.Class");
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public List<T> list(Integer page, Integer limit) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        String requestUrl = this.formatUrl(getUrl(), page, limit);
        this.logger.info("Requesting to ["+requestUrl+"]");

        ResponseEntity<OccResponse<T>> response = restTemplate.exchange(
                requestUrl,
                HttpMethod.GET,
                new HttpEntity(headers),
                getType()
        );

        this.logger.info("Request finished with CODE = "+response.getStatusCode());

        return response.getBody().getItems();
    }

    @Override
    public boolean exists(String id) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        try {
            String url = getUrl() + "/" + id;

            this.logger.info("Requesting [GET] [{}] to OCC...", url);

            ResponseEntity<T> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<T>() {
                    });

            this.logger.info("Request finished with CODE = " + response.getStatusCode());
            return true;
        } catch (HttpStatusCodeException e) {
            if (HttpStatus.BAD_REQUEST.equals(e.getStatusCode()) &&
                    this.isOccNotFoundCode(e.getResponseBodyAsString())) {
                return false;
            }
            throw e;
        }
    }

    @Override
    public void create(T t) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        logger.info("Saving [POST] [{}] in OCC...", getUrl());

        ResponseEntity<Object> response = restTemplate.exchange(
                getUrl(),
                HttpMethod.POST,
                new HttpEntity(t, headers),
                Object.class);

        logger.info("Request finished with CODE = " + response.getStatusCode());
    }

    @Override
    public void update(T t, String id) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        String url = getUrl() + "/" + id;

        logger.info("Updating [PUT] [{}] in OCC...", url);

        ResponseEntity<Object> response = restTemplate.exchange(
                url,
                HttpMethod.PUT,
                new HttpEntity(t, headers),
                Object.class);

        logger.info("Request finished with CODE = " + response.getStatusCode());
    }

    protected static String formatUrl(String url, Integer offset, Integer limit) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

        params.add("offset", String.valueOf(offset));
        params.add("limit", String.valueOf(limit));

        return UriComponentsBuilder.fromUriString(url).queryParams(params).build().toUriString();
    }


    public abstract String getUrl();

    protected abstract ParameterizedTypeReference<OccResponse<T>> getType();

    public abstract String getOccNotFoundCode();

}