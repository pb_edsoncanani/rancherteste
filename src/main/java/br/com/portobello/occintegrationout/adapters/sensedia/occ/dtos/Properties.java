package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos;

import java.util.HashMap;

public class Properties extends HashMap<String, Object> {

    public Boolean getValueBoolean(String key) {
        return (Boolean) get(key);
    }

    public String getValue(String key) {
        return (String) get(key);
    }

}
