package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCanalDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexCanalService")
public class ApexCanalServiceImpl extends AbstractApexService<ApexCanalDto> {

    @Value("${app.apex.canal.list.url}")
    private String canalUrl;

    public ApexCanalServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return canalUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexCanalDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexCanalDto>>() {};
    }
}
