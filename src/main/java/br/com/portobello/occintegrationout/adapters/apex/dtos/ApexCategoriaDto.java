package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexCategoriaDto {

    @JsonProperty("cod_categoria")
    private String codCategoria;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("cod_canal")
    private String codCanal;

    @JsonProperty("ativo")
    private boolean ativo;

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodCanal() {
        return codCanal;
    }

    public void setCodCanal(String codCanal) {
        this.codCanal = codCanal;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public String toString() {
        return "ApexCategoriaDto{" +
                "codCategoria='" + codCategoria + '\'' +
                ", nome='" + nome + '\'' +
                ", codCanal='" + codCanal + '\'' +
                ", ativo=" + ativo +
                '}';
    }
}
