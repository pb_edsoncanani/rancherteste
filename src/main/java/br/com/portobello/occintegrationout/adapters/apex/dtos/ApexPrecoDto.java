package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexPrecoDto {

    @JsonProperty("cod_tabela")
    private String codTabela;

    @JsonProperty("valor")
    private Double valor;

    public String getCodTabela() {
        return codTabela;
    }

    public void setCodTabela(String codTabela) {
        this.codTabela = codTabela;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "ApexPrecoDto{" +
                "codTabela='" + codTabela + '\'' +
                ", valor=" + valor +
                '}';
    }
}
