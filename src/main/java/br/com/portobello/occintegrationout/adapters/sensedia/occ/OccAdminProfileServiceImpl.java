package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.util.UriUtils;

import java.util.List;

@Service
@Qualifier("OccAdminProfileService")
public class OccAdminProfileServiceImpl extends AbstractOccService<OccProfileDto> {

    private Logger logger = LoggerFactory.getLogger(OccAdminProfileServiceImpl.class);

    @Value("${app.sensedia.profile.url}")
    private String occProfileUrl;

    public OccAdminProfileServiceImpl(@Autowired SensediaAuthentication authentication,
                                      @Autowired RestTemplateBuilder builder) {
        super(authentication, builder);
    }

    public String getProfileIdFromEbsId(String idEbs) throws HttpStatusCodeException {
        try {
            List<OccProfileDto> response = getProfileByXIdEbs(idEbs);
            if (!response.isEmpty() && response.get(0).getxIdEbs().equalsIgnoreCase(idEbs)) {
                return response.get(0).getId();
            }
            return idEbs;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    @Override
    public List<OccProfileDto> list(Integer page, Integer limit) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        String qParam =  "x_is_admin eq \"true\"";
        String requestUrl = this.formatUrl(getUrl(), page * limit, limit);
        requestUrl = requestUrl + "&q="+qParam+"&useAdvancedQParser=true";
        this.logger.info("Requesting to ["+requestUrl+"]");

        ResponseEntity<OccResponse<OccProfileDto>> response = restTemplate.exchange(
                requestUrl,
                HttpMethod.GET,
                new HttpEntity(headers),
                getType()
        );

        this.logger.info("Request finished with CODE = "+response.getStatusCode());

        return response.getBody().getItems();
    }

    private List<OccProfileDto> getProfileByXIdEbs(String idEbs){
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        try {
            String qParam =  "x_id_ebs eq \"" + idEbs + "\"";
            String url = getUrl() + "?q="+qParam+"&useAdvancedQParser=true";

            this.logger.info("Requesting [GET] [{}] to OCC...", url);

            ResponseEntity<OccResponse<OccProfileDto>> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<OccResponse<OccProfileDto>>() {
                    });
            this.logger.info("Request finished with CODE = " + response.getStatusCode());
            return response.getBody().getItems();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    @Override
    public String getUrl() {
        return occProfileUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccProfileDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccProfileDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "22002";
    }

}
