package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCategoriaDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexCategoriaService")
public class ApexCategoriaServiceImpl extends AbstractApexService<ApexCategoriaDto> {

    @Value("${app.apex.categorias.list.url}")
    private String categoriasUrl;

    public ApexCategoriaServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return categoriasUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexCategoriaDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexCategoriaDto>>() {};
    }
}
