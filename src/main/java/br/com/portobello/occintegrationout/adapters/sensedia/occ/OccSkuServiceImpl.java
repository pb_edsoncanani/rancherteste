package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.sku.OccSkuDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("OccSkuService")
public class OccSkuServiceImpl extends AbstractOccService<OccSkuDto> {

    private Logger logger = LoggerFactory.getLogger(OccSkuServiceImpl.class);

    @Value("${app.sensedia.skus.url}")
    private String occSkusUrl;

    public OccSkuServiceImpl(@Autowired RestTemplateBuilder builder,
                             @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    @Override
    public String getUrl() {
        return occSkusUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccSkuDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccSkuDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "26060";
    }


}
