package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexCabecalhoPrecoDto {

    @JsonProperty("cod_tabela")
    private String codTabela;

    public String getCodTabela() {
        return codTabela;
    }

    public void setCodTabela(String codTabela) {
        this.codTabela = codTabela;
    }

    @Override
    public String toString() {
        return "ApexCabecalhoPrecoDto{" +
                "codTabela='" + codTabela + '\'' +
                '}';
    }
}
