package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.sku;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccSkuDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("productId")
    private String productId;

    @JsonProperty("active")
    private boolean active = true;

    @JsonProperty("quantity")
    private Long quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OccSkuDto{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", active=" + active +
                ", quantity=" + quantity +
                '}';
    }
}
