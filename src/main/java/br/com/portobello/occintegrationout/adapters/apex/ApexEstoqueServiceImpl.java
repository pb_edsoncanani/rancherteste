package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexEstoqueDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexEstoqueService")
public class ApexEstoqueServiceImpl extends AbstractApexService<ApexEstoqueDto> {

    @Value("${app.apex.estoques.list.url}")
    private String estoquesUrl;

    public ApexEstoqueServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return estoquesUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexEstoqueDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexEstoqueDto>>() {
        };
    }

}
