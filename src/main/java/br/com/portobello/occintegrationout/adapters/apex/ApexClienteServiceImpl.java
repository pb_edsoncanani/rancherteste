package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCabecalhoPrecoDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexClienteService")
public class ApexClienteServiceImpl extends AbstractApexService<ApexClienteDto> {

    @Value("${app.apex.clientes.list.url}")
    private String clientesUrl;

    public ApexClienteServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return clientesUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexClienteDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexClienteDto>>() {};
    }
}
