package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccPriceDto {

    private Double listPrice;
    private String priceListId;
    private String productId;

    public Double getListPrice() {
        return listPrice;
    }

    public void setListPrice(Double listPrice) {
        this.listPrice = listPrice;
    }

    public String getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "OccPriceDto{" +
                "listPrice=" + listPrice +
                ", priceListId='" + priceListId + '\'' +
                ", productId='" + productId + '\'' +
                '}';
    }
}
