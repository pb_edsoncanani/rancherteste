package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccProductCollectionDto {

    @JsonProperty("collections")
    private List<String> collections = new ArrayList<>();

    public List<String> getCollections() {
        return Collections.unmodifiableList(collections);
    }

    public void add(String value) {
        collections.add(value);
    }

    @Override
    public String toString() {
        return "OccProductCollectionDto{" +
                "collections=" + collections +
                '}';
    }
}
