package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexProdutoDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexProdutoService")
public class ApexProdutoServiceImpl extends AbstractApexService<ApexProdutoDto> {

    @Value("${app.apex.produtos.list.url}")
    private String produtosUrl;

    public ApexProdutoServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return produtosUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexProdutoDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexProdutoDto>>() {
        };
    }

}
