package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OccCatalogDto {

    @JsonProperty("catalogId")
    private String catalogId;

    @JsonProperty("displayName")
    private String displayName;

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "OccCatalogDto{" +
                "catalogId='" + catalogId + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
