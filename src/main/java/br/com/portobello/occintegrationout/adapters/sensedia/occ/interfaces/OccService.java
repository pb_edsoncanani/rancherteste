package br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces;

import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;

public interface OccService<T> {

    boolean exists(String id) throws HttpStatusCodeException;
    void create(T object) throws HttpStatusCodeException;
    void update(T object, String id) throws HttpStatusCodeException;
    List<T> list(Integer page, Integer limit) throws HttpStatusCodeException;

}
