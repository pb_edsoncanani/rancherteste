package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OccUpdateProductDto {

    @JsonProperty("properties")
    private OccUpdateProductPropertiesDto properties;

    public OccUpdateProductPropertiesDto getProperties() {
        return properties;
    }

    public void setProperties(OccUpdateProductPropertiesDto properties) {
        this.properties = properties;
    }
}
