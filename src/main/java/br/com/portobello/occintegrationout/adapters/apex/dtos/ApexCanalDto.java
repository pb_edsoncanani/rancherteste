package br.com.portobello.occintegrationout.adapters.apex.dtos;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexCanalDto {

    @JsonProperty("cod_canal")
    private String codCanal;

    @JsonProperty("canal")
    private String canal;

    public String getCodCanal() {
        return codCanal;
    }

    public void setCodCanal(String codCanal) {
        this.codCanal = codCanal;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    @Override
    public String toString() {
        return "ApexCanalDto{" +
                "codCanal='" + codCanal + '\'' +
                ", canal='" + canal + '\'' +
                '}';
    }
}
