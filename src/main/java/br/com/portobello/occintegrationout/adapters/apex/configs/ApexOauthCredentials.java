package br.com.portobello.occintegrationout.adapters.apex.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.stereotype.Component;

@Component
public class ApexOauthCredentials {

    @Value("${app.apex.oauth2.url}")
    private String url;

    @Value("${app.apex.oauth2.client.id}")
    private String clientId;

    @Value("${app.apex.oauth2.client.secret}")
    private String clientSecret;

    @Value("${app.apex.oauth2.grantType}")
    private String grantType;

    public OAuth2ProtectedResourceDetails credentials() {
        ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        details.setAccessTokenUri(url);
        details.setAuthenticationScheme(AuthenticationScheme.header);
        details.setGrantType(grantType);
        return details;
    }
}
