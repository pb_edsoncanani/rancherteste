package br.com.portobello.occintegrationout.adapters.sensedia.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

@Component
public class SensediaOauthCredentials {

    @Value("${app.sensedia.oauth2.url}")
    private String sensediaOauthURL;

    @Value("${app.sensedia.oauth2.client_id}")
    private String sensediaClientId;

    @Value("${app.sensedia.oauth2.client_secret}")
    private String sensediaClientSecret;

    @Value("${app.sensedia.oauth2.username}")
    private String sensediaUsername;

    @Value("${app.sensedia.oauth2.password}")
    private String sensediaPassword;

    public String getSensediaOauthURL() {
        return sensediaOauthURL;
    }

    public String getSensediaUsername() {
        return sensediaUsername;
    }

    public String getSensediaPassword() {
        return sensediaPassword;
    }

    public String getSensediaClientId() {
        return sensediaClientId;
    }

    public String getSensediaClientSecret() {
        return sensediaClientSecret;
    }


}
