package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexRepresentanteDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexRepresentanteService")
public class ApexRepresentanteServiceImpl extends AbstractApexService<ApexRepresentanteDto> {

    @Value("${app.apex.representantes.list.url}")
    private String representantesUrl;

    public ApexRepresentanteServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return representantesUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexRepresentanteDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexRepresentanteDto>>() {};
    }
}
