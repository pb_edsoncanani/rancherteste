package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog.OccCatalogDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
@Qualifier("OccCatalogService")
public class OccCatalogServiceImpl extends AbstractOccService<OccCatalogDto> {

    @Value("${app.sensedia.catalog.url}")
    private String catalogUrl;

    private Logger logger = LoggerFactory.getLogger(OccCatalogServiceImpl.class);

    public OccCatalogServiceImpl(@Autowired RestTemplateBuilder builder,
                                 @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    @Override
    public String getUrl() {
        return catalogUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccCatalogDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccCatalogDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "200105";
    }


}
