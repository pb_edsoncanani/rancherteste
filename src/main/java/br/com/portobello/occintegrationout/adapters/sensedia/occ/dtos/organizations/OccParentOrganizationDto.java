package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccParentOrganizationDto {

    public OccParentOrganizationDto() {
    }

    public OccParentOrganizationDto(String id) {
        this.id = id;
    }

    @JsonProperty("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "OccParentOrganizationDto{" +
                "id='" + id + '\'' +
                '}';
    }
}
