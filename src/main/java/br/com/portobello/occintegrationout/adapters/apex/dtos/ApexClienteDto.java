package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexClienteDto {

    @JsonProperty("codigo_cliente")
    private String codigoCliente;

    @JsonProperty("status_cliente")
    private String statusCliente;

    @JsonProperty("cod_grupo")
    private String codGrupo;

    @JsonProperty("grupo_cliente")
    private String grupoCliente;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("cod_ramo_negocio")
    private String codRamoNegocio;

    @JsonProperty("ramo_negocio")
    private String ramoNegocio;

    @JsonProperty("categoria_cliente")
    private String categoriaCliente;

    @JsonProperty("cod_canal")
    private String codCanal;

    @JsonProperty("nome_canal")
    private String nomeCanal;

    @JsonProperty("sites")
    private List<ApexClienteSitesDto> sites;

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getStatusCliente() {
        return statusCliente;
    }

    public void setStatusCliente(String statusCliente) {
        this.statusCliente = statusCliente;
    }

    public String getCodGrupo() {
        return codGrupo;
    }

    public void setCodGrupo(String codGrupo) {
        this.codGrupo = codGrupo;
    }

    public String getGrupoCliente() {
        return grupoCliente;
    }

    public void setGrupoCliente(String grupoCliente) {
        this.grupoCliente = grupoCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodRamoNegocio() {
        return codRamoNegocio;
    }

    public void setCodRamoNegocio(String codRamoNegocio) {
        this.codRamoNegocio = codRamoNegocio;
    }

    public String getRamoNegocio() {
        return ramoNegocio;
    }

    public void setRamoNegocio(String ramoNegocio) {
        this.ramoNegocio = ramoNegocio;
    }

    public String getCategoriaCliente() {
        return categoriaCliente;
    }

    public void setCategoriaCliente(String categoriaCliente) {
        this.categoriaCliente = categoriaCliente;
    }

    public String getCodCanal() {
        return codCanal;
    }

    public void setCodCanal(String codCanal) {
        this.codCanal = codCanal;
    }

    public String getNomeCanal() {
        return nomeCanal;
    }

    public void setNomeCanal(String nomeCanal) {
        this.nomeCanal = nomeCanal;
    }

    public List<ApexClienteSitesDto> getSites() {
        return sites;
    }

    public void setSites(List<ApexClienteSitesDto> sites) {
        this.sites = sites;
    }

    @Override
    public String toString() {
        return "ApexClienteDto{" +
                "codigoCliente='" + codigoCliente + '\'' +
                ", statusCliente='" + statusCliente + '\'' +
                ", codGrupo='" + codGrupo + '\'' +
                ", grupoCliente='" + grupoCliente + '\'' +
                ", nome='" + nome + '\'' +
                ", codRamoNegocio='" + codRamoNegocio + '\'' +
                ", ramoNegocio='" + ramoNegocio + '\'' +
                ", categoriaCliente='" + categoriaCliente + '\'' +
                ", codCanal='" + codCanal + '\'' +
                ", nomeCanal='" + nomeCanal + '\'' +
                ", sites=" + sites +
                '}';
    }
}
