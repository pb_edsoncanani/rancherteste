package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexClienteSitesDto {

    @JsonProperty("location")
    private String location;

    @JsonProperty("site_use_code")
    private String siteUseCode;

    @JsonProperty("rua")
    private String rua;

    @JsonProperty("complemento")
    private String complemento;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("bairro")
    private String bairro;

    @JsonProperty("cep")
    private String cep;

    @JsonProperty("cidade")
    private String cidade;

    @JsonProperty("uf")
    private String uf;

    @JsonProperty("pais")
    private String pais;

    @JsonProperty("status_uso")
    private String statusUso;

    @JsonProperty("status_endereco")
    private String statusEndereco;

    @JsonProperty("representante")
    private ApexClienteRepresentanteDto representante;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSiteUseCode() {
        return siteUseCode;
    }

    public void setSiteUseCode(String siteUseCode) {
        this.siteUseCode = siteUseCode;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getStatusUso() {
        return statusUso;
    }

    public void setStatusUso(String statusUso) {
        this.statusUso = statusUso;
    }

    public String getStatusEndereco() {
        return statusEndereco;
    }

    public void setStatusEndereco(String statusEndereco) {
        this.statusEndereco = statusEndereco;
    }

    public ApexClienteRepresentanteDto getRepresentante() {
        return representante;
    }

    public void setRepresentante(ApexClienteRepresentanteDto representante) {
        this.representante = representante;
    }

    @Override
    public String toString() {
        return "ApexClienteSitesDto{" +
                "location='" + location + '\'' +
                ", siteUseCode='" + siteUseCode + '\'' +
                ", rua='" + rua + '\'' +
                ", complemento='" + complemento + '\'' +
                ", numero='" + numero + '\'' +
                ", bairro='" + bairro + '\'' +
                ", cep='" + cep + '\'' +
                ", cidade='" + cidade + '\'' +
                ", uf='" + uf + '\'' +
                ", pais='" + pais + '\'' +
                ", statusUso='" + statusUso + '\'' +
                ", statusEndereco='" + statusEndereco + '\'' +
                ", representante=" + representante +
                '}';
    }
}
