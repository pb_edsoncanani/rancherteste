package br.com.portobello.occintegrationout.adapters.apex.interfaces;

import org.springframework.web.client.HttpStatusCodeException;

import java.time.Instant;
import java.util.List;

public interface ApexService<T> {

    List<T> list(Integer page, Integer limit, Instant dateOfLastExecution) throws HttpStatusCodeException;
}
