package br.com.portobello.occintegrationout.adapters.sensedia.report.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusReport {

    private String id;

    private String status;

    private String integrationType;

    @JsonSerialize(using = DateTimeSerializer.class)
    private Instant startDate;

    @JsonSerialize(using = DateTimeSerializer.class)
    private Instant endDate;

    private Integer totalRecords;

    private Integer totalRecordsSuccessfully;

    private Integer totalErrorRecords;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalRecordsSuccessfully() {
        return totalRecordsSuccessfully;
    }

    public void setTotalRecordsSuccessfully(Integer totalRecordsSuccessfully) {
        this.totalRecordsSuccessfully = totalRecordsSuccessfully;
    }

    public Integer getTotalErrorRecords() {
        return totalErrorRecords;
    }

    public void setTotalErrorRecords(Integer totalErrorRecords) {
        this.totalErrorRecords = totalErrorRecords;
    }
}
