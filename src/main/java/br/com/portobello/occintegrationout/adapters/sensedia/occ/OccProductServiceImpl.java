package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductCollectionDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Qualifier("OccProductService")
public class OccProductServiceImpl extends AbstractOccService<OccProductDto> {

    private Logger logger = LoggerFactory.getLogger(OccProductServiceImpl.class);

    @Value("${app.sensedia.products.url}")
    private String occProductsUrl;

    public OccProductServiceImpl(@Autowired RestTemplateBuilder builder,
                                 @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    @Override
    public String getUrl() {
        return occProductsUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccProductDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccProductDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "20031";
    }

    public void addCollection(OccProductCollectionDto occProductCollectionDto, String id) {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        String url = getUrl() + "/" + id + "/collections";

        logger.info("Requesting [POST] [{}] to OCC...", url);
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST,
                new HttpEntity(occProductCollectionDto, headers), Object.class);
        logger.info("Request finished with CODE = " + response.getStatusCode());
    }

}
