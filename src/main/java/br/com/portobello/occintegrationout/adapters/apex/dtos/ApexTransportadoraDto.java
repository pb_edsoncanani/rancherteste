package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexTransportadoraDto {

    @JsonProperty("nome_transportadora")
    private String nomeTransportadora;

    @JsonProperty("metodo_entrega")
    private String metodoEntrega;

    @JsonProperty("depositos")
    private String depositos;

    @JsonProperty("tipo")
    private String tipo;

    @JsonProperty("cnpj")
    private String cnpj;

    @JsonProperty("inscricao_estadual")
    private String inscricaoEstadual;

    @JsonProperty("cidade")
    private String cidade;

    @JsonProperty("uf")
    private String uf;

    @JsonProperty("endereco")
    private String endereco;

    @JsonProperty("contato")
    private String contato;

    @JsonProperty("fone_contato")
    private String foneContato;

    @JsonProperty("email_contato")
    private String emailContato;

    public String getNomeTransportadora() {
        return nomeTransportadora;
    }

    public void setNomeTransportadora(String nomeTransportadora) {
        this.nomeTransportadora = nomeTransportadora;
    }

    public String getMetodoEntrega() {
        return metodoEntrega;
    }

    public void setMetodoEntrega(String metodoEntrega) {
        this.metodoEntrega = metodoEntrega;
    }

    public String getDepositos() {
        return depositos;
    }

    public void setDepositos(String depositos) {
        this.depositos = depositos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getFoneContato() {
        return foneContato;
    }

    public void setFoneContato(String foneContato) {
        this.foneContato = foneContato;
    }

    public String getEmailContato() {
        return emailContato;
    }

    public void setEmailContato(String emailContato) {
        this.emailContato = emailContato;
    }

    @Override
    public String toString() {
        return "ApexTransportadoraDto{" +
                "nomeTransportadora='" + nomeTransportadora + '\'' +
                ", metodoEntrega='" + metodoEntrega + '\'' +
                ", depositos='" + depositos + '\'' +
                ", tipo='" + tipo + '\'' +
                ", cnpj='" + cnpj + '\'' +
                ", inscricaoEstadual='" + inscricaoEstadual + '\'' +
                ", cidade='" + cidade + '\'' +
                ", uf='" + uf + '\'' +
                ", endereco='" + endereco + '\'' +
                ", contato='" + contato + '\'' +
                ", foneContato='" + foneContato + '\'' +
                ", emailContato='" + emailContato + '\'' +
                '}';
    }
}
