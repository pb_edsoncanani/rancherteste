package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.inventory.OccInventoryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
@Qualifier("OccInventoryService")
public class OccInventoryServiceImpl extends AbstractOccService<OccInventoryDto> {

    private Logger logger = LoggerFactory.getLogger(OccInventoryServiceImpl.class);

    @Value("${app.sensedia.inventory.url}")
    private String occInventoryUrl;

    public OccInventoryServiceImpl(@Autowired RestTemplateBuilder builder,
                                   @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    @Override
    public String getUrl() {
        return occInventoryUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccInventoryDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccInventoryDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "25126";
    }


}
