package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccBillingAddressDto {

    @JsonProperty("isDefaultBillingAddress")
    private boolean isDefaultBillingAddress;

    @JsonProperty("country")
    private String country;

    @JsonProperty("phoneNumber")
    private String phoneNumber;

    @JsonProperty("city")
    private String city;

    @JsonProperty("postalCode")
    private String postalCode;

    @JsonProperty("companyName")
    private String companyName;

    @JsonProperty("isDefaultShippingAddress")
    private boolean isDefaultShippingAddress;

    @JsonProperty("state")
    private String state;

    @JsonProperty("address1")
    private String address1;

    @JsonProperty("address2")
    private String address2;

    @JsonProperty("address3")
    private String address3;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public boolean getIsDefaultBillingAddress() {
        return isDefaultBillingAddress;
    }

    public void setIsDefaultBillingAddress(boolean isDefaultBillingAddress) {
        this.isDefaultBillingAddress = isDefaultBillingAddress;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public boolean getIsDefaultShippingAddress() {
        return isDefaultShippingAddress;
    }

    public void setIsDefaultShippingAddress(boolean isDefaultShippingAddress) {
        this.isDefaultShippingAddress = isDefaultShippingAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "OccBillingAddressDto{" +
                "isDefaultBillingAddress='" + isDefaultBillingAddress + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", companyName='" + companyName + '\'' +
                ", isDefaultShippingAddress='" + isDefaultShippingAddress + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
