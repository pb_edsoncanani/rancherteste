package br.com.portobello.occintegrationout.adapters.sensedia.report;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.AbstractOccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.dtos.StatusReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
public class StatusReportService {

    private final RestTemplate restTemplate;
    private Logger logger = LoggerFactory.getLogger(AbstractOccService.class);

    @Value("${app.sensedia.reports.url}")
    private String sensediaReportsUrl;

    private SensediaAuthentication authentication;

    public StatusReportService(@Autowired RestTemplateBuilder builder,
                               @Autowired SensediaAuthentication authentication) {
        this.authentication = authentication;
        this.restTemplate = builder.build();
    }

    public void send(StatusReport statusReport) {
        this.logger.info("Sending status report to sensedia");
        this.logger.info("Requesting [POST] ["+sensediaReportsUrl+"] to OCC...");

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    sensediaReportsUrl,
                    HttpMethod.POST,
                    new HttpEntity(statusReport, authentication.getAuthHeader()),
                    String.class);

            this.logger.info("Request finished with CODE = "+response.getStatusCode());
        } catch (HttpStatusCodeException e) {
            this.logger.error(e.getResponseBodyAsString());
        }
    }
}
