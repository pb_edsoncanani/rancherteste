package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup.OccPriceListActivationDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup.OccPriceListGroupDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
@Qualifier("OccPriceListGroupService")
public class OccPriceLisGroupsServiceImpl extends AbstractOccService<OccPriceListGroupDto> {

    private Logger logger = LoggerFactory.getLogger(OccPriceLisGroupsServiceImpl.class);

    @Value("${app.sensedia.priceListGroup.url}")
    private String occPriceListGroupUrl;

    @Value("${app.sensedia.priceListGroup.activationUrl}")
    private String activationUrl;

    public OccPriceLisGroupsServiceImpl(@Autowired RestTemplateBuilder builder,
                                        @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    @Override
    public String getUrl() {
        return occPriceListGroupUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccPriceListGroupDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccPriceListGroupDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "20187";
    }

    public void activate(OccPriceListActivationDto activationDto) {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        logger.info("Activating [POST] [{}] in OCC...", activationUrl);

        ResponseEntity<Object> response = restTemplate.exchange(
                activationUrl,
                HttpMethod.POST,
                new HttpEntity(activationDto, headers),
                Object.class);

        logger.info("Request finished with CODE = " + response.getStatusCode());
    }


}
