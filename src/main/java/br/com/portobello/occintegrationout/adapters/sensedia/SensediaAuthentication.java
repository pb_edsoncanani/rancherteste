package br.com.portobello.occintegrationout.adapters.sensedia;

import br.com.portobello.occintegrationout.adapters.sensedia.configs.SensediaOauthCredentials;
import br.com.portobello.occintegrationout.adapters.sensedia.dtos.SensediaOauthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Service
public class SensediaAuthentication {

    private Logger logger = LoggerFactory.getLogger(SensediaAuthentication.class);

    private String accessToken;
    private String refreshToken;
    private LocalDateTime accessTokenExpiration;
    protected final RestTemplate restTemplate;

    @Autowired
    private SensediaOauthCredentials oauthCredentials;

    public SensediaAuthentication(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    private String authenticate() {
        if (this.accessToken == null || LocalDateTime.now().plusSeconds(30).isAfter(accessTokenExpiration)) {
            logger.info("Creating Sensedia accessToken ...");

            generateAccessToken(oauthCredentials.getSensediaClientId(), oauthCredentials.getSensediaClientSecret(), oauthCredentials.getSensediaOauthURL());

            logger.info("AccessToken will expire at [{}], accessToken [{}]", accessTokenExpiration, accessToken);
        }

        return this.accessToken;
    }

    private void generateAccessToken(String clientId, String clientSecret, String url) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("username", oauthCredentials.getSensediaUsername());
        map.add("password", oauthCredentials.getSensediaPassword());

        this.makeRequest(clientId, clientSecret, map, url);
    }

    private void refreshAccessToken(String clientId, String clientSecret, String refreshToken, String url) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", refreshToken);

        this.makeRequest(clientId, clientSecret, map, url);
    }

    private void makeRequest(String clientId, String clientSecret, MultiValueMap<String, String> map, String url){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(clientId, clientSecret);

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, headers);

        try {
            SensediaOauthResponse response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, SensediaOauthResponse.class).getBody();
            this.accessToken = response.getAccessToken();
            this.refreshToken = response.getRefreshToken();
            this.accessTokenExpiration = LocalDateTime.now().plusSeconds(response.getExpiresIn());
        } catch (HttpStatusCodeException e) {
            this.logger.error("request finished with "+ e.getRawStatusCode());
            this.logger.error("Error in generating Access Token on Sensedia");
            this.logger.error(e.getResponseBodyAsString());
            throw new HttpServerErrorException(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    public HttpHeaders getAuthHeader() {
        HttpHeaders headers = new HttpHeaders();

        String accessToken = this.authenticate();
        headers.add("access_token", accessToken);
        headers.add("client_id", oauthCredentials.getSensediaClientId());

        return headers;
    }
}
