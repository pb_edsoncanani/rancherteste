package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexAdministradoresDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexAdministradoresService")
public class ApexAdministradoresServiceImpl extends AbstractApexService<ApexAdministradoresDto> {

    @Value("${app.apex.administradores.list.url}")
    private String administradoresUrl;

    public ApexAdministradoresServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return administradoresUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexAdministradoresDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexAdministradoresDto>>() {};
    }
}
