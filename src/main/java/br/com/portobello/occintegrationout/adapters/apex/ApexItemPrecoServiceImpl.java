package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexItemPrecoDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexItemPrecoService")
public class ApexItemPrecoServiceImpl extends AbstractApexService<ApexItemPrecoDto> {

    @Value("${app.apex.itemPreco.list.url}")
    private String canalUrl;

    public ApexItemPrecoServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return canalUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexItemPrecoDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexItemPrecoDto>>() {
        };
    }
}
