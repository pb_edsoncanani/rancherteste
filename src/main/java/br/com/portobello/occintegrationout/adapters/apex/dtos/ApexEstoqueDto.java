package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexEstoqueDto {

    @JsonProperty("cod_deposito")
    private String codDeposito;

    @JsonProperty("cod_produto")
    private String codProduto;

    @JsonProperty("cod_tonalidade_calibre")
    private String codTonalidadeCalibre;

    @JsonProperty("qtd_disponivel")
    private Integer qtdDisponivel;

    public String getCodDeposito() {
        return codDeposito;
    }

    public void setCodDeposito(String codDeposito) {
        this.codDeposito = codDeposito;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public String getCodTonalidadeCalibre() {
        return codTonalidadeCalibre;
    }

    public void setCodTonalidadeCalibre(String codTonalidadeCalibre) {
        this.codTonalidadeCalibre = codTonalidadeCalibre;
    }

    public Integer getQtdDisponivel() {
        return qtdDisponivel;
    }

    public void setQtdDisponivel(Integer qtdDisponivel) {
        this.qtdDisponivel = qtdDisponivel;
    }

    @Override
    public String toString() {
        return "ApexEstoqueDto{" +
                "codDeposito='" + codDeposito + '\'' +
                ", codProduto='" + codProduto + '\'' +
                ", codTonalidadeCalibre='" + codTonalidadeCalibre + '\'' +
                ", qtdDisponivel=" + qtdDisponivel +
                '}';
    }
}
