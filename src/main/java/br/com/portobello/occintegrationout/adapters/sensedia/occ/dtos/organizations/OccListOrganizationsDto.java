package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccListOrganizationsDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("billingAddress")
    private OccBillingAddressDto billingAddress;

    @JsonProperty("members")
    private List<OccMembersDto> members = new ArrayList<>();

    @JsonProperty("parentOrganization")
    private OccParentOrganizationDto parentOrganization;

    @JsonProperty("x_id_ebs")
    private String xIdEbs;

    @JsonProperty("dynamicPropertyNotAdminIds")
    private String dynamicPropertyNotAdminIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OccBillingAddressDto getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(OccBillingAddressDto billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<OccMembersDto> getMembers() {
        return members;
    }

    public void setMembers(List<OccMembersDto> members) {
        this.members = members;
    }

    public OccParentOrganizationDto getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(OccParentOrganizationDto parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public String getxIdEbs() {
        return xIdEbs;
    }

    public void setxIdEbs(String xIdEbs) {
        this.xIdEbs = xIdEbs;
    }

    public String getDynamicPropertyNotAdminIds() {
        return dynamicPropertyNotAdminIds;
    }

    public void setDynamicPropertyNotAdminIds(String dynamicPropertyNotAdminIds) {
        this.dynamicPropertyNotAdminIds = dynamicPropertyNotAdminIds;
    }

    @Override
    public String toString() {
        return "OccListOrganizationsDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", billingAddress=" + billingAddress +
                ", members=" + members +
                ", parentOrganization=" + parentOrganization +
                ", xIdEbs='" + xIdEbs + '\'' +
                ", dynamicPropertyNotAdminIds='" + dynamicPropertyNotAdminIds + '\'' +
                '}';
    }
}
