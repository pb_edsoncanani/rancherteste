package br.com.portobello.occintegrationout.adapters.sensedia.report;

import br.com.portobello.occintegrationout.adapters.sensedia.report.dtos.StatusReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class StatusReportServiceAsync {

    private final StatusReportService statusReportService;

    @Autowired
    public StatusReportServiceAsync(StatusReportService statusReportService) {
        this.statusReportService = statusReportService;
    }

    @Async
    public void send(StatusReport statusReport) {
        statusReportService.send(statusReport);
    }
}
