package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.collections.OccCollectionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
@Qualifier("OccCollectionService")
public class OccCollectionServiceImpl extends AbstractOccService<OccCollectionDto> {

    private Logger logger = LoggerFactory.getLogger(OccCollectionServiceImpl.class);

    @Value("${app.sensedia.collection.url}")
    private String occCollectionUrl;

    public OccCollectionServiceImpl(@Autowired RestTemplateBuilder builder,
                                    @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }


    @Override
    public String getUrl() {
        return occCollectionUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccCollectionDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccCollectionDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return null;
    }


}
