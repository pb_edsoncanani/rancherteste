package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCabecalhoPrecoDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ApexCabecalhoPrecoService")
public class ApexCabecalhoPrecoServiceImpl extends AbstractApexService<ApexCabecalhoPrecoDto> {

    @Value("${app.apex.cabecalhoPrecos.list.url}")
    private String cabecalhoPrecoUrl;

    public ApexCabecalhoPrecoServiceImpl(@Autowired ApexOauthCredentials apexOauthCredentials) {
        super(apexOauthCredentials);
    }

    @Override
    protected String getUrl() {
        return cabecalhoPrecoUrl;
    }

    @Override
    protected ParameterizedTypeReference<ApexResponse<ApexCabecalhoPrecoDto>> getType() {
        return new ParameterizedTypeReference<ApexResponse<ApexCabecalhoPrecoDto>>() {};
    }
}
