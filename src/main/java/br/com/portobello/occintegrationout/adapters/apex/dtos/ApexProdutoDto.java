package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexProdutoDto {

    @JsonProperty("cod_produto")
    private String codProduto;

    @JsonProperty("produto")
    private String produto;

    @JsonProperty("unidade")
    private String unidade;

    @JsonProperty("imagem_produto")
    private String imagemProduto;

    @JsonProperty("sufixo")
    private String sufixo;

    @JsonProperty("desc_sufixo")
    private String descSufixo;

    @JsonProperty("fase_vida")
    private String faseVida;

    @JsonProperty("formato")
    private String formato;

    @JsonProperty("cod_tipologia_cml")
    private Integer codTipologiaCml;

    @JsonProperty("linha")
    private String linha;

    @JsonProperty("aplicacao_tecnica")
    private String aplicacaoTecnica;

    @JsonProperty("multiplo_venda")
    private Double multiploVenda;

    @JsonProperty("m2_caixa")
    private Double m2Caixa;

    @JsonProperty("pc_caixa")
    private Integer pcCaixa;

    @JsonProperty("m2_por_peca")
    private Double m2PorPeca;

    @JsonProperty("peso_bruto_caixa")
    private String pesoBrutoCaixa;

    @JsonProperty("peso_bruto_pallete")
    private String pesoBrutoPallete;

    @JsonProperty("camada_por_pallete")
    private Integer camadaPorPallete;

    @JsonProperty("pc_pallets")
    private Integer pcPallets;

    @JsonProperty("nr_faces")
    private Integer nrFaces;

    @JsonProperty("marca_item")
    private String marcaItem;

    @JsonProperty("origem_item")
    private String origemItem;

    @JsonProperty("ppe")
    private String ppe;

    @JsonProperty("canal_vendas")
    private List<String> canalVendas;

    @JsonProperty("cor")
    private String cor;

    @JsonProperty("link_download")
    private String linkDownload;

    @JsonProperty("precos")
    private List<ApexPrecoDto> precos;

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getImagemProduto() {
        return imagemProduto;
    }

    public void setImagemProduto(String imagemProduto) {
        this.imagemProduto = imagemProduto;
    }

    public String getSufixo() {
        return sufixo;
    }

    public void setSufixo(String sufixo) {
        this.sufixo = sufixo;
    }

    public String getDescSufixo() {
        return descSufixo;
    }

    public void setDescSufixo(String descSufixo) {
        this.descSufixo = descSufixo;
    }

    public String getFaseVida() {
        return faseVida;
    }

    public void setFaseVida(String faseVida) {
        this.faseVida = faseVida;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Integer getCodTipologiaCml() {
        return codTipologiaCml;
    }

    public void setCodTipologiaCml(Integer codTipologiaCml) {
        this.codTipologiaCml = codTipologiaCml;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getAplicacaoTecnica() {
        return aplicacaoTecnica;
    }

    public void setAplicacaoTecnica(String aplicacaoTecnica) {
        this.aplicacaoTecnica = aplicacaoTecnica;
    }

    public Double getMultiploVenda() {
        return multiploVenda;
    }

    public void setMultiploVenda(Double multiploVenda) {
        this.multiploVenda = multiploVenda;
    }

    public Double getM2Caixa() {
        return m2Caixa;
    }

    public void setM2Caixa(Double m2Caixa) {
        this.m2Caixa = m2Caixa;
    }

    public Integer getPcCaixa() {
        return pcCaixa;
    }

    public void setPcCaixa(Integer pcCaixa) {
        this.pcCaixa = pcCaixa;
    }

    public Double getM2PorPeca() {
        return m2PorPeca;
    }

    public void setM2PorPeca(Double m2PorPeca) {
        this.m2PorPeca = m2PorPeca;
    }

    public String getPesoBrutoCaixa() {
        return pesoBrutoCaixa;
    }

    public void setPesoBrutoCaixa(String pesoBrutoCaixa) {
        this.pesoBrutoCaixa = pesoBrutoCaixa;
    }

    public String getPesoBrutoPallete() {
        return pesoBrutoPallete;
    }

    public void setPesoBrutoPallete(String pesoBrutoPallete) {
        this.pesoBrutoPallete = pesoBrutoPallete;
    }

    public Integer getCamadaPorPallete() {
        return camadaPorPallete;
    }

    public void setCamadaPorPallete(Integer camadaPorPallete) {
        this.camadaPorPallete = camadaPorPallete;
    }

    public Integer getPcPallets() {
        return pcPallets;
    }

    public void setPcPallets(Integer pcPallets) {
        this.pcPallets = pcPallets;
    }

    public Integer getNrFaces() {
        return nrFaces;
    }

    public void setNrFaces(Integer nrFaces) {
        this.nrFaces = nrFaces;
    }

    public String getMarcaItem() {
        return marcaItem;
    }

    public void setMarcaItem(String marcaItem) {
        this.marcaItem = marcaItem;
    }

    public String getOrigemItem() {
        return origemItem;
    }

    public void setOrigemItem(String origemItem) {
        this.origemItem = origemItem;
    }

    public String getPpe() {
        return ppe;
    }

    public void setPpe(String ppe) {
        this.ppe = ppe;
    }

    public List<String> getCanalVendas() {
        return canalVendas;
    }

    public void setCanalVendas(List<String> canalVendas) {
        this.canalVendas = canalVendas;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }

    public List<ApexPrecoDto> getPrecos() {
        return precos;
    }

    public void setPrecos(List<ApexPrecoDto> precos) {
        this.precos = precos;
    }

    @Override
    public String toString() {
        return "ApexProdutoDto{" +
                "codProduto='" + codProduto + '\'' +
                ", produto='" + produto + '\'' +
                ", unidade='" + unidade + '\'' +
                ", imagemProduto='" + imagemProduto + '\'' +
                ", sufixo='" + sufixo + '\'' +
                ", descSufixo='" + descSufixo + '\'' +
                ", faseVida='" + faseVida + '\'' +
                ", formato='" + formato + '\'' +
                ", codTipologiaCml=" + codTipologiaCml +
                ", linha='" + linha + '\'' +
                ", aplicacaoTecnica='" + aplicacaoTecnica + '\'' +
                ", multiploVenda=" + multiploVenda +
                ", m2Caixa=" + m2Caixa +
                ", pcCaixa=" + pcCaixa +
                ", m2PorPeca=" + m2PorPeca +
                ", pesoBrutoCaixa='" + pesoBrutoCaixa + '\'' +
                ", pesoBrutoPallete='" + pesoBrutoPallete + '\'' +
                ", camadaPorPallete=" + camadaPorPallete +
                ", pcPallets=" + pcPallets +
                ", nrFaces=" + nrFaces +
                ", marcaItem='" + marcaItem + '\'' +
                ", origemItem='" + origemItem + '\'' +
                ", ppe='" + ppe + '\'' +
                ", canalVendas='" + canalVendas + '\'' +
                ", cor='" + cor + '\'' +
                ", linkDownload='" + linkDownload + '\'' +
                ", precos=" + precos +
                '}';
    }
}