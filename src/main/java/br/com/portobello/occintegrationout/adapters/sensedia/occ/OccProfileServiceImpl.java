package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.util.UriUtils;

import java.util.List;

@Service
@Qualifier("OccProfileService")
public class OccProfileServiceImpl extends AbstractOccService<OccProfileDto> {

    private Logger logger = LoggerFactory.getLogger(OccProfileServiceImpl.class);

    @Value("${app.sensedia.profile.url}")
    private String occProfileUrl;

    public OccProfileServiceImpl(@Autowired SensediaAuthentication authentication,
                                 @Autowired RestTemplateBuilder builder) {
        super(authentication, builder);
    }

    public String getProfileIdFromEbsId(String idEbs) throws HttpStatusCodeException {
        try {
            List<OccProfileDto> response = getProfileByXIdEbs(idEbs);
            if (!response.isEmpty() && response.get(0).getxIdEbs().equalsIgnoreCase(idEbs)) {
                return response.get(0).getId();
            }
            return idEbs;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }


    private List<OccProfileDto> getProfileByXIdEbs(String idEbs){
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        try {
            String qParam =  "x_id_ebs eq \"" + idEbs + "\"";
            String url = getUrl() + "?q="+qParam+"&useAdvancedQParser=true";

            this.logger.info("Requesting [GET] [{}] to OCC...", url);

            ResponseEntity<OccResponse<OccProfileDto>> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<OccResponse<OccProfileDto>>() {
                    });
            this.logger.info("Request finished with CODE = " + response.getStatusCode());
            return response.getBody().getItems();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public String createAndReturnId(OccProfileDto occProfileDto) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        logger.info("Saving [POST] [{}] in OCC...", getUrl());

        ResponseEntity<OccProfileDto> response = restTemplate.exchange(
                getUrl(),
                HttpMethod.POST,
                new HttpEntity(occProfileDto, headers),
                OccProfileDto.class);

        logger.info("Request finished with CODE = " + response.getStatusCode());
        String profileId = response.getBody().getId();
        logger.info("Profile created and returned id [{}]", profileId);
        return profileId;
    }

    @Override
    public String getUrl() {
        return occProfileUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccProfileDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccProfileDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "22002";
    }
}
