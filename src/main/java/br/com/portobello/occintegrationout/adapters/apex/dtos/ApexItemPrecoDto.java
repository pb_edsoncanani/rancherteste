package br.com.portobello.occintegrationout.adapters.apex.dtos;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexItemPrecoDto {

    @JsonProperty("cod_tabela")
    private String codTabela;

    @JsonProperty("cod_produto")
    private String codProduto;

    @JsonProperty("preco")
    private String preco;

    public String getCodTabela() {
        return codTabela;
    }

    public void setCodTabela(String codTabela) {
        this.codTabela = codTabela;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "ApexItemPrecoDto{" +
                "codTabela='" + codTabela + '\'' +
                ", codProduto='" + codProduto + '\'' +
                ", preco='" + preco + '\'' +
                '}';
    }
}
