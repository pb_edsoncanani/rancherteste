package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexRepresentanteDto {

    @JsonProperty("email")
    private String email;

    @JsonProperty("primeiro_nome")
    private String primeiroNome;

    @JsonProperty("ultimo_nome")
    private String ultimoNome;

    @JsonProperty("cod_canal")
    private String codCanal;

    @JsonProperty("nome_canal")
    private String nomeCanal;

    @JsonProperty("cod_representante")
    private String codRepresentante;

    @JsonProperty("ativo")
    private String ativo;

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getUltimoNome() {
        return ultimoNome;
    }

    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }

    public String getCodCanal() {
        return codCanal;
    }

    public void setCodCanal(String codCanal) {
        this.codCanal = codCanal;
    }

    public String getNomeCanal() {
        return nomeCanal;
    }

    public void setNomeCanal(String nomeCanal) {
        this.nomeCanal = nomeCanal;
    }

    public String getCodRepresentante() {
        return codRepresentante;
    }

    public void setCodRepresentante(String codRepresentante) {
        this.codRepresentante = codRepresentante;
    }

    @Override
    public String toString() {
        return "ApexRepresentanteDto{" +
                "email='" + email + '\'' +
                ", primeiroNome='" + primeiroNome + '\'' +
                ", ultimoNome='" + ultimoNome + '\'' +
                ", codCanal='" + codCanal + '\'' +
                ", nomeCanal='" + nomeCanal + '\'' +
                ", codRepresentante='" + codRepresentante + '\'' +
                ", ativo='" + ativo + '\'' +
                '}';
    }
}
