package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccListOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.util.UriUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@Qualifier("OccOrganizationService")
public class OccOrganizationServiceImpl extends AbstractOccService<OccOrganizationsDto> {

    private Logger logger = LoggerFactory.getLogger(OccOrganizationServiceImpl.class);

    @Value("${app.sensedia.organization.url}")
    private String occOrganizationUrl;

    public OccOrganizationServiceImpl(@Autowired SensediaAuthentication authentication,
                                      @Autowired RestTemplateBuilder builder) {
        super(authentication, builder);
    }

    public List<OccListOrganizationsDto> listOcc(Integer page, Integer limit) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        try {
            String url = formatUrl(getUrl(), page * limit, limit);

            this.logger.info("Requesting [GET] [{}] to OCC...", url);

            ResponseEntity<OccResponse<OccListOrganizationsDto>> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<OccResponse<OccListOrganizationsDto>>() {
                    });
            this.logger.info("Request finished with CODE = " + response.getStatusCode());
            return response.getBody().getItems();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    public String getOrganizationsIdFromEbsId(String idEbs) throws HttpStatusCodeException {
        try {
            List<OccOrganizationsDto> response = getOrganizationsByXIdEbs(idEbs);
            if (!response.isEmpty() && response.get(0).getxIdEbs().equalsIgnoreCase(idEbs)) {
                return response.get(0).getId();
            }
            return idEbs;
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    private List<OccOrganizationsDto> getOrganizationsByXIdEbs(String idEbs){
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        try {
            String qParam =  UriUtils.encode("x_id_ebs eq \"" + idEbs + "\"", "UTF-8");
            String url = getUrl() + "?q="+qParam;

            this.logger.info("Requesting [GET] [{}] to OCC...", url);

            ResponseEntity<OccResponse<OccOrganizationsDto>> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<OccResponse<OccOrganizationsDto>>() {
                    });
            this.logger.info("Request finished with CODE = " + response.getStatusCode());
            return response.getBody().getItems();
        } catch (HttpStatusCodeException e) {
            throw e;
        }
    }

    @Override
    public String getUrl() {
        return occOrganizationUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccOrganizationsDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccOrganizationsDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return "100001";
    }

    public String createAndReturnId(OccOrganizationsDto occDto) throws HttpStatusCodeException{
            HttpHeaders headers = authentication.getAuthHeader();
            headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

            logger.info("Saving [POST] [{}] in OCC...", getUrl());

            ResponseEntity<OccOrganizationsDto> response = restTemplate.exchange(
                    getUrl(),
                    HttpMethod.POST,
                    new HttpEntity(occDto, headers),
                    OccOrganizationsDto.class);

            logger.info("Request finished with CODE = " + response.getStatusCode());
            String organizationsId = response.getBody().getId();
            logger.info("Organization created and returned id [{}]", organizationsId);
            return organizationsId;
    }
}
