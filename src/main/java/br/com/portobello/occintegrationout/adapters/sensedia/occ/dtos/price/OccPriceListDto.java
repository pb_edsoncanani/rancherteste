package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccPriceListDto {

    private List<OccPriceDto> items = new ArrayList<>();

    public List<OccPriceDto> getItems() {
        return Collections.unmodifiableList(items);
    }

    public OccPriceListDto add(OccPriceDto occPriceDto) {
        items.add(occPriceDto);
        return this;
    }

    @Override
    public String toString() {
        return "OccPriceListDto{" +
                "items=" + items +
                '}';
    }
}
