package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccProductPropertiesDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("displayName")
    private String displayName;

    @JsonProperty("x_unidade")
    private String xUnidade;

    @JsonProperty("primaryFullImageURL")
    private String primaryFullImageURL;

    @JsonProperty("primaryLargeImageURL")
    private String primaryLargeImageURL;

    @JsonProperty("x_sufixo")
    private String xSufixo;

    @JsonProperty("description")
    private String description;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("x_formato")
    private String xFormato;

    @JsonProperty("x_linha")
    private String xLinha;

    @JsonProperty("xAplicacaoTecnica")
    private String xAplicacaoTecnica;

    @JsonProperty("x_multiplo_venda")
    private Double xMultiploVenda;

    @JsonProperty("x_m2_caixa")
    private Double xM2Caixa;

    @JsonProperty("x_pc_caixa")
    private Integer xPcCaixa;

    @JsonProperty("x_m2_por_peca")
    private Double xM2PorPeca;

    @JsonProperty("weight")
    private Double weight;

    @JsonProperty("x_peso_bruto_pallete")
    private String xPesoBrutoPallete;

    @JsonProperty("x_camada_por_pallete")
    private Integer xCamadaPorPallete;

    @JsonProperty("x_pc_pallets")
    private Integer xPcPallets;

    @JsonProperty("x_nr_faces")
    private Integer xNrFaces;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("x_origem_item")
    private String xOrigemItem;

    @JsonProperty("x_ppe")
    private String xPpe;

    @JsonProperty("x_cor")
    private String xCor;

    @JsonProperty("listPrices")
    private Map<String, Double> listPrices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getxUnidade() {
        return xUnidade;
    }

    public void setxUnidade(String xUnidade) {
        this.xUnidade = xUnidade;
    }

    public String getPrimaryFullImageURL() {
        return primaryFullImageURL;
    }

    public void setPrimaryFullImageURL(String primaryFullImageURL) {
        this.primaryFullImageURL = primaryFullImageURL;
    }

    public String getPrimaryLargeImageURL() {
        return primaryLargeImageURL;
    }

    public void setPrimaryLargeImageURL(String primaryLargeImageURL) {
        this.primaryLargeImageURL = primaryLargeImageURL;
    }

    public String getxSufixo() {
        return xSufixo;
    }

    public void setxSufixo(String xSufixo) {
        this.xSufixo = xSufixo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getxFormato() {
        return xFormato;
    }

    public void setxFormato(String xFormato) {
        this.xFormato = xFormato;
    }

    public String getxLinha() {
        return xLinha;
    }

    public void setxLinha(String xLinha) {
        this.xLinha = xLinha;
    }

    public String getxAplicacaoTecnica() {
        return xAplicacaoTecnica;
    }

    public void setxAplicacaoTecnica(String xAplicacaoTecnica) {
        this.xAplicacaoTecnica = xAplicacaoTecnica;
    }

    public Double getxMultiploVenda() {
        return xMultiploVenda;
    }

    public void setxMultiploVenda(Double xMultiploVenda) {
        this.xMultiploVenda = xMultiploVenda;
    }

    public Double getxM2Caixa() {
        return xM2Caixa;
    }

    public void setxM2Caixa(Double xM2Caixa) {
        this.xM2Caixa = xM2Caixa;
    }

    public Integer getxPcCaixa() {
        return xPcCaixa;
    }

    public void setxPcCaixa(Integer xPcCaixa) {
        this.xPcCaixa = xPcCaixa;
    }

    public Double getxM2PorPeca() {
        return xM2PorPeca;
    }

    public void setxM2PorPeca(Double xM2PorPeca) {
        this.xM2PorPeca = xM2PorPeca;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getxPesoBrutoPallete() {
        return xPesoBrutoPallete;
    }

    public void setxPesoBrutoPallete(String xPesoBrutoPallete) {
        this.xPesoBrutoPallete = xPesoBrutoPallete;
    }

    public Integer getxCamadaPorPallete() {
        return xCamadaPorPallete;
    }

    public void setxCamadaPorPallete(Integer xCamadaPorPallete) {
        this.xCamadaPorPallete = xCamadaPorPallete;
    }

    public Integer getxPcPallets() {
        return xPcPallets;
    }

    public void setxPcPallets(Integer xPcPallets) {
        this.xPcPallets = xPcPallets;
    }

    public Integer getxNrFaces() {
        return xNrFaces;
    }

    public void setxNrFaces(Integer xNrFaces) {
        this.xNrFaces = xNrFaces;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getxOrigemItem() {
        return xOrigemItem;
    }

    public void setxOrigemItem(String xOrigemItem) {
        this.xOrigemItem = xOrigemItem;
    }

    public String getxPpe() {
        return xPpe;
    }

    public void setxPpe(String xPpe) {
        this.xPpe = xPpe;
    }

    public String getxCor() {
        return xCor;
    }

    public void setxCor(String xCor) {
        this.xCor = xCor;
    }

    public Map<String, Double> getListPrices() {
        return listPrices;
    }

    public void setListPrices(Map<String, Double> listPrices) {
        this.listPrices = listPrices;
    }

    @Override
    public String toString() {
        return "OccProductPropertiesDto{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", xUnidade='" + xUnidade + '\'' +
                ", primaryFullImageURL='" + primaryFullImageURL + '\'' +
                ", primaryLargeImageURL='" + primaryLargeImageURL + '\'' +
                ", xSufixo='" + xSufixo + '\'' +
                ", description='" + description + '\'' +
                ", active=" + active +
                ", xFormato='" + xFormato + '\'' +
                ", xLinha='" + xLinha + '\'' +
                ", xAplicacaoTecnica='" + xAplicacaoTecnica + '\'' +
                ", xMultiploVenda=" + xMultiploVenda +
                ", xM2Caixa=" + xM2Caixa +
                ", xPcCaixa=" + xPcCaixa +
                ", xM2PorPeca=" + xM2PorPeca +
                ", weight=" + weight +
                ", xPesoBrutoPallete='" + xPesoBrutoPallete + '\'' +
                ", xCamadaPorPallete=" + xCamadaPorPallete +
                ", xPcPallets=" + xPcPallets +
                ", xNrFaces=" + xNrFaces +
                ", brand='" + brand + '\'' +
                ", xOrigemItem='" + xOrigemItem + '\'' +
                ", xPpe='" + xPpe + '\'' +
                ", xCor='" + xCor + '\'' +
                ", listPrices=" + listPrices +
                '}';
    }
}
