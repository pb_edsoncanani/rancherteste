package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccProfileDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("email")
    private String email;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("receiveEmail")
    private String receiveEmail;

    @JsonProperty("x_nome_canal")
    private String xNomeCanal;

    @JsonProperty("x_codigo_representante")
    private String xCodigoRepresentante;

    @JsonProperty("x_id_ebs")
    private String xIdEbs;

    @JsonProperty("x_is_admin")
    private boolean xIsAdmin;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("profileType")
    private String profileType;

    public String isProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getxIdEbs() {
        return xIdEbs;
    }

    public void setxIdEbs(String xIdEbs) {
        this.xIdEbs = xIdEbs;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getReceiveEmail() {
        return receiveEmail;
    }

    public void setReceiveEmail(String receiveEmail) {
        this.receiveEmail = receiveEmail;
    }

    public String getxNomeCanal() {
        return xNomeCanal;
    }

    public void setxNomeCanal(String xNomeCanal) {
        this.xNomeCanal = xNomeCanal;
    }

    public String getxCodigoRepresentante() {
        return xCodigoRepresentante;
    }

    public void setxCodigoRepresentante(String xCodigoRepresentante) {
        this.xCodigoRepresentante = xCodigoRepresentante;
    }

    public boolean getxIsAdmin() {
        return xIsAdmin;
    }

    public void setxIsAdmin(boolean xIsAdmin) {
        this.xIsAdmin = xIsAdmin;
    }

    @Override
    public String toString() {
        return "OccProfileDto{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", receiveEmail='" + receiveEmail + '\'' +
                ", xNomeCanal='" + xNomeCanal + '\'' +
                ", xCodigoRepresentante='" + xCodigoRepresentante + '\'' +
                ", xIdEbs='" + xIdEbs + '\'' +
                ", xIsAdmin=" + xIsAdmin +
                ", active=" + active +
                '}';
    }
}
