package br.com.portobello.occintegrationout.adapters.sensedia.occ;

import br.com.portobello.occintegrationout.adapters.sensedia.SensediaAuthentication;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.OccResponse;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price.OccPriceListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
@Qualifier("OccPriceService")
public class OccPriceServiceImpl extends AbstractOccService<OccPriceListDto> {

    private Logger logger = LoggerFactory.getLogger(OccPriceServiceImpl.class);

    @Value("${app.sensedia.prices.url}")
    private String occPriceUrl;

    public OccPriceServiceImpl(@Autowired RestTemplateBuilder builder,
                               @Autowired SensediaAuthentication authentication) {
        super(authentication, builder);
    }

    public void update(OccPriceListDto occPriceDto) throws HttpStatusCodeException {
        HttpHeaders headers = authentication.getAuthHeader();
        headers.set(HEADER_X_CCASSET_LANGUAGE, xOccAssetLanguage);

        String url = getUrl();

        logger.info("Updating [PUT] [{}] in OCC...", url);

        ResponseEntity<Object> response = restTemplate.exchange(
                url,
                HttpMethod.PUT,
                new HttpEntity(occPriceDto, headers),
                Object.class);

        logger.info("Request finished with CODE = " + response.getStatusCode());
    }

    @Override
    public String getUrl() {
        return occPriceUrl;
    }

    @Override
    protected ParameterizedTypeReference<OccResponse<OccPriceListDto>> getType() {
        return new ParameterizedTypeReference<OccResponse<OccPriceListDto>>() {};
    }

    @Override
    public String getOccNotFoundCode() {
        return null;
    }


}
