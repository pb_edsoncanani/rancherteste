package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccMembersDto {

    public OccMembersDto() {
    }

    public OccMembersDto(String id) {
        this.id = id;
    }

    @JsonProperty("id")
    private String id;

    @JsonProperty("repositoryId")
    private String repositoryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    @Override
    public String toString() {
        return "OccMembersDto{" +
                "id='" + id + '\'' +
                ", repositoryId='" + repositoryId + '\'' +
                '}';
    }
}
