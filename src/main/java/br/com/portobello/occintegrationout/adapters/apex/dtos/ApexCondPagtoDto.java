package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexCondPagtoDto {

    @JsonProperty("term_id")
    private Integer termId;

    @JsonProperty("codigo")
    private String codigo;

    @JsonProperty("descricao")
    private String descricao;

    @JsonProperty("dias")
    private String dias;

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    @Override
    public String toString() {
        return "ApexCondPagtoDto{" +
                "termId=" + termId +
                ", codigo='" + codigo + '\'' +
                ", descricao='" + descricao + '\'' +
                ", dias='" + dias + '\'' +
                '}';
    }
}
