package br.com.portobello.occintegrationout.adapters.apex;

import br.com.portobello.occintegrationout.adapters.apex.configs.ApexOauthCredentials;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexResponse;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.Instant;
import java.util.List;

public abstract class AbstractApexService<T> implements ApexService<T> {

    protected final OAuth2RestTemplate oAuth2RestTemplate;
    private Logger logger = LoggerFactory.getLogger(AbstractApexService.class);

    public AbstractApexService(@Autowired ApexOauthCredentials apexOauthCredentials) {
        this.oAuth2RestTemplate = new OAuth2RestTemplate(apexOauthCredentials.credentials());
    }

    public List<T> list(Integer page, Integer limit, Instant dateOfLastExecution) {
        Integer offset = page * limit;

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getUrl())
                .queryParam("offset", offset)
                .queryParam("limit", limit);


        if (dateOfLastExecution != null) {
            builder.queryParam("q", "{\"data_modificacao\": {\"$gt\": {\"$date\":\"" + dateOfLastExecution + "\"}}}");
        }

        URI uri = builder.build().toUri();

        this.logger.info("Requesting to [" + uri + "]");

        ResponseEntity<ApexResponse<T>> response = oAuth2RestTemplate.exchange(
                uri,
                HttpMethod.GET,
                new HttpEntity(new HttpHeaders()),
                getType()
        );

        this.logger.info("Request finished with CODE = " + response.getStatusCode());

        return response.getBody().getItems();
    }

    protected abstract String getUrl();

    protected abstract ParameterizedTypeReference<ApexResponse<T>> getType();
}
