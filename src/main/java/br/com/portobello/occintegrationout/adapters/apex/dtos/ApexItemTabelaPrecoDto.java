package br.com.portobello.occintegrationout.adapters.apex.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApexItemTabelaPrecoDto {

    @JsonProperty("cod_tabela")
    private String codTabela;

    @JsonProperty("list_header_id")
    private Integer listHeaderId;

    @JsonProperty("moeda")
    private String moeda;

    @JsonProperty("inventory_item_id")
    private String inventoryItemId;

    @JsonProperty("cod_produto")
    private String codProduto;

    @JsonProperty("description")
    private String description;

    @JsonProperty("unidade")
    private String unidade;

    @JsonProperty("preco")
    private String preco;

    public String getCodTabela() {
        return codTabela;
    }

    public void setCodTabela(String codTabela) {
        this.codTabela = codTabela;
    }

    public Integer getListHeaderId() {
        return listHeaderId;
    }

    public void setListHeaderId(Integer listHeaderId) {
        this.listHeaderId = listHeaderId;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public String getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(String inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public String getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "ApexItemTabelaPrecoDto{" +
                "codTabela='" + codTabela + '\'' +
                ", listHeaderId=" + listHeaderId +
                ", moeda='" + moeda + '\'' +
                ", inventoryItemId='" + inventoryItemId + '\'' +
                ", codProduto='" + codProduto + '\'' +
                ", description='" + description + '\'' +
                ", unidade='" + unidade + '\'' +
                ", preco='" + preco + '\'' +
                '}';
    }
}
