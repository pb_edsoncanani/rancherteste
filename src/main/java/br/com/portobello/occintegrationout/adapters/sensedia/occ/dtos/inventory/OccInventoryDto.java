package br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.inventory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OccInventoryDto {

    private String id;

    private String stockLevel;

    private String locationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(String stockLevel) {
        this.stockLevel = stockLevel;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @Override
    public String toString() {
        return "OccInventoryDto{" +
                "id='" + id + '\'' +
                ", stockLevel='" + stockLevel + '\'' +
                ", locationId='" + locationId + '\'' +
                '}';
    }
}
