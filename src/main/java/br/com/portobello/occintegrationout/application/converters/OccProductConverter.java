package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexPrecoDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexProdutoDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductPropertiesDto;
import br.com.portobello.occintegrationout.exceptions.ConversionFailedException;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;

@Component
public class OccProductConverter implements OccConverter<OccProductDto, ApexProdutoDto> {

    public static final String VALUE_FOR_ACTIVE_PRODUCT = "AT";

    @Override
    public OccProductDto convert(ApexProdutoDto apexProdutoDto) {
        OccProductDto occProductDto = new OccProductDto();
        occProductDto.setProperties(new OccProductPropertiesDto());

        occProductDto.getProperties().setId(apexProdutoDto.getCodProduto());
        occProductDto.getProperties().setDisplayName(apexProdutoDto.getProduto());
        occProductDto.getProperties().setxUnidade(apexProdutoDto.getUnidade());
        occProductDto.getProperties().setPrimaryFullImageURL(apexProdutoDto.getImagemProduto());
        occProductDto.getProperties().setPrimaryLargeImageURL(apexProdutoDto.getImagemProduto());
        occProductDto.getProperties().setxSufixo(apexProdutoDto.getSufixo());
        occProductDto.getProperties().setDescription(apexProdutoDto.getDescSufixo());
        occProductDto.getProperties().setxFormato(apexProdutoDto.getFormato());
        occProductDto.getProperties().setxLinha(apexProdutoDto.getLinha());
        occProductDto.getProperties().setxAplicacaoTecnica(apexProdutoDto.getAplicacaoTecnica());
        occProductDto.getProperties().setxMultiploVenda(apexProdutoDto.getMultiploVenda());
        occProductDto.getProperties().setxM2Caixa(apexProdutoDto.getM2Caixa());
        occProductDto.getProperties().setxPcCaixa(apexProdutoDto.getPcCaixa());
        occProductDto.getProperties().setxM2PorPeca(apexProdutoDto.getM2PorPeca());
        occProductDto.getProperties().setxPesoBrutoPallete(apexProdutoDto.getPesoBrutoPallete());
        occProductDto.getProperties().setxCamadaPorPallete(apexProdutoDto.getCamadaPorPallete());
        occProductDto.getProperties().setxPcPallets(apexProdutoDto.getPcPallets());
        occProductDto.getProperties().setxNrFaces(apexProdutoDto.getNrFaces());
        occProductDto.getProperties().setBrand(apexProdutoDto.getMarcaItem());
        occProductDto.getProperties().setxOrigemItem(apexProdutoDto.getOrigemItem());
        occProductDto.getProperties().setxPpe(apexProdutoDto.getPpe());
        occProductDto.getProperties().setxCor(apexProdutoDto.getCor());

        try {
            occProductDto.getProperties().setWeight(DecimalFormat.getNumberInstance().parse(apexProdutoDto.getPesoBrutoCaixa()).doubleValue());
        } catch (ParseException e) {
            throw new ConversionFailedException("Failed to convert '{}' to Double", apexProdutoDto.getPesoBrutoCaixa());
        }

        if (VALUE_FOR_ACTIVE_PRODUCT.equalsIgnoreCase(apexProdutoDto.getFaseVida())) {
            occProductDto.getProperties().setActive(true);
        }

        occProductDto.getProperties().setListPrices(new HashMap<>());
        for (ApexPrecoDto apexPrecoDto : apexProdutoDto.getPrecos()) {
            occProductDto.getProperties().getListPrices().put(apexPrecoDto.getCodTabela(), apexPrecoDto.getValor());
        }

        return occProductDto;
    }
}
