package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexAdministradoresDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteSitesDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccAdminProfileServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccOrganizationServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccProfileServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccMembersDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccParentOrganizationDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccChildOrganizationConverter;
import br.com.portobello.occintegrationout.application.converters.OccOrganizationConverter;
import br.com.portobello.occintegrationout.application.converters.OccProfileConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class OrganizationApplication extends AbstractApplication<OccOrganizationsDto, ApexClienteDto> implements IntegrationManager {

    private static final String PORTOBELLO_SHOP_COD_CANAL = "4";
    private static final String PORTOBELLO_SHOP_COD_GRUPO1 = "25";
    private static final String PORTOBELLO_SHOP_COD_GRUPO2 = "26";

    private Logger logger = LoggerFactory.getLogger(ProfileApplication.class);
    private Set<OccProfileDto> adminList = new HashSet<>();
    private final OccProfileServiceImpl occProfileService;
    private final OccAdminProfileServiceImpl occAdminProfileService;
    private final OccOrganizationServiceImpl occOrganizationService;
    private final OccProfileConverter occProfileConverter;
    private final OccChildOrganizationConverter occChildOrganizationConverter;

    public OrganizationApplication(@Qualifier("ApexClienteService") ApexService<ApexClienteDto> apexService,
                                   @Qualifier("OccOrganizationService") OccOrganizationServiceImpl occService,
                                   @Qualifier("OccProfileService") OccProfileServiceImpl occProfileService,
                                   @Qualifier("OccAdminProfileService") OccAdminProfileServiceImpl occAdminProfileService,
                                   IntegrationControlRepository repository,
                                   StatusReportServiceAsync statusReportService,
                                   OccOrganizationConverter converter,
                                   OccProfileConverter occProfileConverter,
                                   OccChildOrganizationConverter occChildOrganizationConverter,
                                   @Value("${app.apex.clientes.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occProfileService = occProfileService;
        this.occOrganizationService = occService;
        this.occAdminProfileService = occAdminProfileService;
        this.occProfileConverter = occProfileConverter;
        this.occChildOrganizationConverter = occChildOrganizationConverter;
    }

    @Override
    protected void doBeforeAll() {
        populateAdminList();
    }

    @Override
    protected void doBeforeUpdating(OccOrganizationsDto occDto, ApexClienteDto apexDto) {
        doBefore(occDto, apexDto);
    }

    @Override
    protected void doBeforeCreating(OccOrganizationsDto occDto, ApexClienteDto apexDto) {
        doBefore(occDto, apexDto);
    }

    @Override
    protected void sendToOcc(OccOrganizationsDto occDto, ApexClienteDto apexDto) {
        String organizationId = occOrganizationService.getOrganizationsIdFromEbsId(occDto.getxIdEbs());
        if (occOrganizationService.exists(organizationId)) {
            doBeforeUpdating(occDto, apexDto);
            logger.info("Calling OCC update operation.");
            occOrganizationService.update(occDto, organizationId);
            occDto.setId(organizationId);
            doAfterUpdating(occDto, apexDto);
        } else {
            doBeforeCreating(occDto, apexDto);
            logger.info("Calling OCC create operation.");
            organizationId = occOrganizationService.createAndReturnId(occDto);
            occDto.setId(organizationId);
            doAfterCreating(occDto, apexDto);
        }
    }

    @Override
    protected void doAfterCreating(OccOrganizationsDto parentOrganization, ApexClienteDto apexDto) {
        addClienteSites(parentOrganization, apexDto.getSites());
    }

    @Override
    protected void doAfterUpdating(OccOrganizationsDto parentOrganization, ApexClienteDto apexDto) {
        addClienteSites(parentOrganization, apexDto.getSites());
    }

    @Override
    protected String getId(OccOrganizationsDto occDto) {
        return occDto.getxIdEbs();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.ORGANIZATIONS;
    }

    private void doBefore(OccOrganizationsDto occDto, ApexClienteDto apexDto){
        // If cliente is from Portobello Shop create/update profile
        if (PORTOBELLO_SHOP_COD_CANAL.equalsIgnoreCase(apexDto.getCodCanal()) && (PORTOBELLO_SHOP_COD_GRUPO1.equalsIgnoreCase(apexDto.getCodGrupo()) || PORTOBELLO_SHOP_COD_GRUPO2.equalsIgnoreCase(apexDto.getCodGrupo()))){
            String profileCreatedId = addProfile(occProfileConverter.convert(apexDto));
            occDto.addMember(profileCreatedId);
        }
        for (OccProfileDto admins: this.adminList) {
            occDto.addMember(admins.getId());
        }
    }

    private String addProfile(OccProfileDto occDto){
        String profileCreatedId = occProfileService.getProfileIdFromEbsId(occDto.getxIdEbs());
        if (occProfileService.exists(profileCreatedId)) {
            logger.info("Calling OCC update operation.");
            occProfileService.update(occDto, profileCreatedId);
        } else {
            logger.info("Calling OCC create operation.");
            profileCreatedId = occProfileService.createAndReturnId(occDto);
        }
        return profileCreatedId;
    }

    private void populateAdminList() {
        // Populate admin list
        logger.info("Starting populating administrators list");
        List<OccProfileDto> occList;
        Integer page = 0;
        do {
            logger.info("Requesting OCC with the values: page [{}], limit [{}]",
                    page, limit);
            occList = occAdminProfileService.list(page, limit);

            for (OccProfileDto occAdminProfile : occList) {
                adminList.add(occAdminProfile);
            }
            page++;
        } while (!occList.isEmpty());
        logger.info("Finishing administrators list");
    }

    private void addClienteSites(OccOrganizationsDto parentOrganization, List<ApexClienteSitesDto> clienteSites) {
        for (ApexClienteSitesDto clienteSite: clienteSites) {
            // ADD/UPDATE CHILD ORGANIZATIONS
            String represProfileId = findProfileIdByCodRepres(clienteSite.getRepresentante().getCodigo());
            String parentOrganizationId = parentOrganization.getId();

            OccOrganizationsDto childOrganizationDto = occChildOrganizationConverter.convert(clienteSite);
            childOrganizationDto.addMember(represProfileId);
            childOrganizationDto.setParentOrganization(new OccParentOrganizationDto(parentOrganizationId));

            createChildOrganization(childOrganizationDto);
        }
    }

    private void createChildOrganization(OccOrganizationsDto childOrganizationDto) {
        String organizationsId = occOrganizationService.getOrganizationsIdFromEbsId(childOrganizationDto.getxIdEbs());
        if (occOrganizationService.exists(organizationsId)) {
            logger.info("Calling OCC update operation.");
            occOrganizationService.update(childOrganizationDto, organizationsId);
        } else {
            logger.info("Calling OCC create operation.");
            occOrganizationService.create(childOrganizationDto);
        }
    }

    private String findProfileIdByCodRepres(String codigoRepresentante) {
        return occProfileService.getProfileIdFromEbsId(codigoRepresentante);
    }

}