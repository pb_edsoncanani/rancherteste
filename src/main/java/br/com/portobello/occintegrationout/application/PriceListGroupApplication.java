package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCabecalhoPrecoDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccPriceLisGroupsServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup.OccPriceListActivationDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup.OccPriceListGroupDto;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccPriceListGroupConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PriceListGroupApplication extends AbstractApplication<OccPriceListGroupDto, ApexCabecalhoPrecoDto> implements IntegrationManager {

    private OccPriceLisGroupsServiceImpl occService;

    public PriceListGroupApplication(@Qualifier("ApexCabecalhoPrecoService") ApexService<ApexCabecalhoPrecoDto> apexService,
                                     OccPriceLisGroupsServiceImpl occService,
                                     IntegrationControlRepository repository,
                                     StatusReportServiceAsync statusReportService,
                                     OccPriceListGroupConverter converter,
                                     @Value("${app.apex.cabecalhoPrecos.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occService = occService;
    }

    @Override
    protected void doAfterCreating(OccPriceListGroupDto occDto, ApexCabecalhoPrecoDto apexDto) {
        occService.activate(new OccPriceListActivationDto(occDto.getId()));
    }

    @Override
    protected void doAfterUpdating(OccPriceListGroupDto occDto, ApexCabecalhoPrecoDto apexDto) {
        occService.activate(new OccPriceListActivationDto(occDto.getId()));
    }

    @Override
    protected void doBeforeUpdating(OccPriceListGroupDto occDto, ApexCabecalhoPrecoDto apexDto) {
        occDto.setLocale(null);
    }

    @Override
    protected void doBeforeCreating(OccPriceListGroupDto occDto, ApexCabecalhoPrecoDto apexDto) {
    }

    @Override
    protected String getId(OccPriceListGroupDto occDto) {
        return occDto.getId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.PRICE_LIST_GROUP;
    }
}
