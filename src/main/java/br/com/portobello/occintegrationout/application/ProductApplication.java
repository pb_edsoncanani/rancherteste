package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.ApexProdutoServiceImpl;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexProdutoDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccProductServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccSkuServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductCollectionDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.sku.OccSkuDto;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccProductConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProductApplication extends AbstractApplication<OccProductDto, ApexProdutoDto> implements IntegrationManager {

    private Logger logger = LoggerFactory.getLogger(ProductApplication.class);

    private OccProductServiceImpl occProductService;

    public ProductApplication(
            ApexProdutoServiceImpl apexService,
            OccProductServiceImpl occService,
            IntegrationControlRepository repository,
            StatusReportServiceAsync statusReportService,
            OccProductConverter converter,
            @Value("${app.apex.produtos.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occProductService = occService;
    }

    @Override
    protected void doAfterCreating(OccProductDto occDto, ApexProdutoDto apexDto) {
        addCollection(occDto, apexDto);
    }

    @Override
    protected void doAfterUpdating(OccProductDto occDto, ApexProdutoDto apexDto) {
        addCollection(occDto, apexDto);
    }

    private void addCollection(OccProductDto occDto, ApexProdutoDto apexDto) {
        if (apexDto.getCanalVendas() == null || apexDto.getCanalVendas().isEmpty()) {
            logger.warn("No sales channels associated with product [{}]", getId(occDto));
            return;
        }

        OccProductCollectionDto occProductCollectionDto = new OccProductCollectionDto();

        for (String idCanalVendas: apexDto.getCanalVendas()) {
            occProductCollectionDto.add(idCanalVendas + "-" + String.valueOf(apexDto.getCodTipologiaCml()));
        }

        logger.info("add product in collection. Object [{}]", occProductCollectionDto);
        occProductService.addCollection(occProductCollectionDto, getId(occDto));
    }

    @Override
    protected void doBeforeUpdating(OccProductDto occDto, ApexProdutoDto apexDto) {
    }

    @Override
    protected void doBeforeCreating(OccProductDto occDto, ApexProdutoDto apexDto) {
    }

    @Override
    protected String getId(OccProductDto occDto) {
        return occDto.getProperties().getId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.PRODUCTS;
    }
}
