package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Component
public class UserFlowApplication implements IntegrationManager {
    private Logger logger = LoggerFactory.getLogger(UserFlowApplication.class);

    private final ProfileApplication profileApplication;
    private final OrganizationApplication organizationApplication;
    private final AdminProfileApplication adminProfileApplication;



    public UserFlowApplication(@Autowired ProfileApplication profileApplication,
                               @Autowired OrganizationApplication organizationApplication,
                               @Autowired AdminProfileApplication adminProfileApplication) {
        this.profileApplication = profileApplication;
        this.organizationApplication = organizationApplication;
        this.adminProfileApplication = adminProfileApplication;
    }

    @Override
    public void sync() {
        try {
            adminProfileApplication.sync();
//            profileApplication.sync();
//            organizationApplication.sync();
        } catch (HttpStatusCodeException e) {
            logger.error("Error sending to OCC. StatusCode [{}], Response [{}]", e.getRawStatusCode(),
                    e.getResponseBodyAsString());
        } catch (OAuth2AccessDeniedException e) {
            logger.error("Failure on requesting access token.", e);
        } catch (Exception e) {
            logger.error("An unexpected error has occurred", e);
        }
    }
}
