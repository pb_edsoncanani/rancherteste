package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexEstoqueDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.sku.OccSkuDto;
import org.springframework.stereotype.Component;

@Component
public class OccSkuConverter implements OccConverter<OccSkuDto, ApexEstoqueDto> {

    public static final long INFINITY_STOCK = 999999999999L;

    @Override
    public OccSkuDto convert(ApexEstoqueDto apexDto) {
        OccSkuDto occSkuDto = new OccSkuDto();

        String skuId = apexDto.getCodProduto() + "-" + apexDto.getCodTonalidadeCalibre();

        occSkuDto.setId(skuId);
        occSkuDto.setProductId(apexDto.getCodProduto());
        occSkuDto.setQuantity(INFINITY_STOCK);

        return occSkuDto;
    }
}
