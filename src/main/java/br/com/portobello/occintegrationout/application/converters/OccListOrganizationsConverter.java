package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccListOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccMembersDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import org.springframework.stereotype.Component;

@Component
public class OccListOrganizationsConverter implements OccConverter<OccOrganizationsDto, OccListOrganizationsDto>  {

    @Override
    public OccOrganizationsDto convert(OccListOrganizationsDto occListDto) {
        OccOrganizationsDto occOrganizationsDto = new OccOrganizationsDto();

        occOrganizationsDto.setParentOrganization(occListDto.getParentOrganization());
        occOrganizationsDto.setId(occListDto.getId());
        occOrganizationsDto.setBillingAddress(occListDto.getBillingAddress());
        occOrganizationsDto.setName(occListDto.getName());
        occOrganizationsDto.setxIdEbs(occListDto.getxIdEbs());

        for(OccMembersDto membersDto: occListDto.getMembers()) {
            occOrganizationsDto.addMember(membersDto.getId());
        }

        return occOrganizationsDto;
    }
}
