package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.ApexEstoqueServiceImpl;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexEstoqueDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccSkuServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.sku.OccSkuDto;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccSkuConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SkuApplication extends AbstractApplication<OccSkuDto, ApexEstoqueDto> implements IntegrationManager {

    public SkuApplication(
            ApexEstoqueServiceImpl apexService,
            OccSkuServiceImpl occService,
            IntegrationControlRepository repository,
            StatusReportServiceAsync statusReportService,
            OccSkuConverter converter,
            @Value("${app.apex.estoques.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
    }

    @Override
    protected void doAfterCreating(OccSkuDto occDto, ApexEstoqueDto apexDto) {
    }

    @Override
    protected void doAfterUpdating(OccSkuDto occDto, ApexEstoqueDto apexDto) {
    }

    @Override
    protected void doBeforeUpdating(OccSkuDto occDto, ApexEstoqueDto apexDto) {
    }

    @Override
    protected void doBeforeCreating(OccSkuDto occDto, ApexEstoqueDto apexDto) {
    }

    @Override
    protected String getId(OccSkuDto occDto) {
        return occDto.getId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.SKU;
    }
}
