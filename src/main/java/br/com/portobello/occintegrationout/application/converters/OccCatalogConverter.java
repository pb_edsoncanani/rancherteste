package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCanalDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog.OccCatalogDto;
import org.springframework.stereotype.Component;

@Component
public class OccCatalogConverter implements OccConverter<OccCatalogDto, ApexCanalDto> {

    @Override
    public OccCatalogDto convert(ApexCanalDto apexDto) {
        OccCatalogDto occCatalogDto = new OccCatalogDto();

        occCatalogDto.setCatalogId(apexDto.getCodCanal());
        occCatalogDto.setDisplayName(apexDto.getCanal());

        return occCatalogDto;
    }
}
