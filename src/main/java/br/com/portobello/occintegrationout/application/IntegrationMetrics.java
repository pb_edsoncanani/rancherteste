package br.com.portobello.occintegrationout.application;

public class IntegrationMetrics {

    private Integer totalRecords = 0;

    private Integer totalRecordsSuccessfully = 0;

    private Integer totalErrorRecords = 0;

    public void registerSuccess() {
        totalRecordsSuccessfully++;
        totalRecords++;
    }

    public void registerError() {
        totalErrorRecords++;
        totalRecords++;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public Integer getTotalRecordsSuccessfully() {
        return totalRecordsSuccessfully;
    }

    public Integer getTotalErrorRecords() {
        return totalErrorRecords;
    }
}
