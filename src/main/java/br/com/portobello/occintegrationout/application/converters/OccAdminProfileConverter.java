package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexAdministradoresDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexAdministradoresDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class OccAdminProfileConverter implements OccConverter<OccProfileDto, ApexAdministradoresDto> {
    @Override
    public OccProfileDto convert(ApexAdministradoresDto apexDto) {
        OccProfileDto occProfileDto = new OccProfileDto();

        // TODO remove email
//        occProfileDto.setEmail(apexDto.getEmail());
        occProfileDto.setEmail(getSaltString()+"@gmail.com");
        occProfileDto.setFirstName(apexDto.getPrimeiroNome());
        occProfileDto.setLastName(apexDto.getUltimoNome());
        occProfileDto.setxIdEbs(apexDto.getUserName());
        occProfileDto.setxIsAdmin(true);
        occProfileDto.setActive("true".equalsIgnoreCase(apexDto.getAtivo()));
        occProfileDto.setReceiveEmail("yes");
        occProfileDto.setProfileType("b2b_user");

        return occProfileDto;
    }
    

    protected String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuU1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
