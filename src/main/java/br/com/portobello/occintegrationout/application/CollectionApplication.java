package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCanalDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCategoriaDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog.OccCatalogDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.collections.OccCollectionDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccCollectionConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CollectionApplication extends AbstractApplication<OccCollectionDto, ApexCategoriaDto> implements IntegrationManager {

    private Logger logger = LoggerFactory.getLogger(CollectionApplication.class);

    @Autowired
    public CollectionApplication(@Qualifier("ApexCategoriaService") ApexService<ApexCategoriaDto> apexService,
                                 @Qualifier("OccCollectionService") OccService<OccCollectionDto> occService,
                                 IntegrationControlRepository repository,
                                 StatusReportServiceAsync statusReportService,
                                 OccCollectionConverter converter,
                                 @Value("${app.apex.categorias.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
    }

    @Override
    protected String getId(OccCollectionDto occDto) {
        return occDto.getProperties().getValue("id");
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.COLLECTION;
    }

    @Override
    protected void doAfterCreating(OccCollectionDto occDto, ApexCategoriaDto apexDto) {}

    @Override
    protected void doAfterUpdating(OccCollectionDto occDto, ApexCategoriaDto apexDto) {}

    @Override
    protected void doBeforeUpdating(OccCollectionDto occDto, ApexCategoriaDto apexDto) {

    }

    @Override
    protected void doBeforeCreating(OccCollectionDto occDto, ApexCategoriaDto apexDto) {

    }
}
