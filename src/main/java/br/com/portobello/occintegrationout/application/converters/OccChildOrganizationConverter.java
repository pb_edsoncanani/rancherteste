package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteSitesDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import org.springframework.stereotype.Component;

@Component
public class OccChildOrganizationConverter implements OccConverter<OccOrganizationsDto, ApexClienteSitesDto> {

    @Override
    public OccOrganizationsDto convert(ApexClienteSitesDto apexDto) {
        return null;
    }
}
