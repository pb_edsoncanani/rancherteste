package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.ApexItemPrecoServiceImpl;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexItemPrecoDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccPriceServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price.OccPriceListDto;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccPriceConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PriceApplication extends AbstractApplication<OccPriceListDto, ApexItemPrecoDto> implements IntegrationManager {

    private Logger logger = LoggerFactory.getLogger(PriceApplication.class);

    private OccPriceServiceImpl occService;

    @Autowired
    public PriceApplication(ApexItemPrecoServiceImpl apexService,
                            OccPriceServiceImpl occService,
                            IntegrationControlRepository repository,
                            StatusReportServiceAsync statusReportService,
                            OccPriceConverter converter,
                            @Value("${app.apex.itemPreco.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occService = occService;
    }

    @Override
    protected void sendToOcc(OccPriceListDto occDto, ApexItemPrecoDto apexDto) {
        occService.update(occDto);
    }

    @Override
    protected String getId(OccPriceListDto occDto) {
        return null;
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.PRICE;
    }

    @Override
    protected void doAfterCreating(OccPriceListDto occDto, ApexItemPrecoDto apexDto) {
    }

    @Override
    protected void doAfterUpdating(OccPriceListDto occDto, ApexItemPrecoDto apexDto) {
    }

    @Override
    protected void doBeforeUpdating(OccPriceListDto occDto, ApexItemPrecoDto apexDto) {
    }

    @Override
    protected void doBeforeCreating(OccPriceListDto occDto, ApexItemPrecoDto apexDto) {
    }
}
