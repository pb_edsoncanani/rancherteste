package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexRepresentanteDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccProfileServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccProfileConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProfileApplication extends AbstractApplication<OccProfileDto, ApexRepresentanteDto> implements IntegrationManager {

    private final OccProfileServiceImpl occProfileService;

    public ProfileApplication(@Qualifier("ApexRepresentanteService") ApexService<ApexRepresentanteDto> apexService,
                              @Qualifier("OccProfileService") OccProfileServiceImpl occService,
                              IntegrationControlRepository repository,
                              StatusReportServiceAsync statusReportService,
                              OccProfileConverter converter,
                              @Value("${app.apex.representantes.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occProfileService = occService;
    }

    @Override
    protected String getId(OccProfileDto occDto) {
        if (occDto.getId() == null) {
            String profileId = this.occProfileService.getProfileIdFromEbsId(occDto.getxIdEbs());
            occDto.setId(profileId);
        }
        return occDto.getId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.PROFILE;
    }
}
