package br.com.portobello.occintegrationout.application.converters;

public interface OccConverter<O, A> {

    O convert(A apexDto);
}
