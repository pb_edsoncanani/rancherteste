package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexAdministradoresDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccAdminProfileServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.OccOrganizationServiceImpl;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccListOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccMembersDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccAdminProfileConverter;
import br.com.portobello.occintegrationout.application.converters.OccListOrganizationsConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.exceptions.ConversionFailedException;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AdminProfileApplication extends AbstractApplication<OccProfileDto, ApexAdministradoresDto> implements IntegrationManager {
    private Logger logger = LoggerFactory.getLogger(AdminProfileApplication.class);
    private List<String> oldOccAdminList = new ArrayList<>();
    private List<String> newOccAdminList = new ArrayList<>();
    private List<String> removeFromOcc = new ArrayList<>();
    private List<String> addToOcc = new ArrayList<>();
    private final OccOrganizationServiceImpl occOrganizationsService;
    private final OccAdminProfileServiceImpl occAdminProfileService;
    private final OccListOrganizationsConverter occListOrganizationsConverter;

    public AdminProfileApplication(@Qualifier("ApexAdministradoresService") ApexService<ApexAdministradoresDto> apexService,
                                   @Qualifier("OccAdminProfileService") OccAdminProfileServiceImpl occService,
                                   @Qualifier("OccOrganizationService") OccOrganizationServiceImpl occOrganizationsService,
                                   IntegrationControlRepository repository,
                                   StatusReportServiceAsync statusReportService,
                                   OccAdminProfileConverter converter,
                                   OccListOrganizationsConverter occListOrganizationsConverter,
                                   @Value("${app.apex.administradores.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
        this.occOrganizationsService = occOrganizationsService;
        this.occAdminProfileService = occService;
        this.occListOrganizationsConverter = occListOrganizationsConverter;
    }

    @Override
    protected void doBeforeAll() {
        // Populate admin list
        logger.info("Starting populating administrators list");
        List<OccProfileDto> occList;
        Integer page = 0;
        do {
            logger.info("Requesting OCC with the values: page [{}], limit [{}]",
                    page, limit);
            occList = occAdminProfileService.list(page, limit);
            for (OccProfileDto occAdminProfile : occList) {
                oldOccAdminList.add(occAdminProfile.getId());
            }
            page++;
        } while (!occList.isEmpty());
        logger.info("Administrators list [{}]", oldOccAdminList);
        logger.info("Finishing administrators list");
    }

    @Override
    protected void doAfterCreating(OccProfileDto occDto, ApexAdministradoresDto apexDto) {
        newOccAdminList.add(occDto.getId());
    }

    @Override
    protected void doAfterUpdating(OccProfileDto occDto, ApexAdministradoresDto apexDto) {
        newOccAdminList.add(occDto.getId());
    }

    @Override
    protected void doAfterAll() {
        distinctAdmins();
        disableAdministrators(removeFromOcc);
        updateOrganizationsList();
    }

    @Override
    protected String getId(OccProfileDto occDto) {
        if (occDto.getId() == null) {
            String profileId = this.occAdminProfileService.getProfileIdFromEbsId(occDto.getxIdEbs());
            occDto.setId(profileId);
        }
        return occDto.getId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.ADMIN_PROFILE;
    }

    private void distinctAdmins() {
        logger.info("Starting admins distinct proccess ");
        removeFromOcc = oldOccAdminList.stream().distinct().filter(str -> !newOccAdminList.contains(str)).collect(Collectors.toList());
        addToOcc = newOccAdminList.stream().distinct().filter(str -> !oldOccAdminList.contains(str)).collect(Collectors.toList());
        logger.info("Admins to remove from OCC [{}]", removeFromOcc);
        logger.info("Admins to add to OCC [{}]", addToOcc);
        logger.info("Finishing admins distinct proccess");
    }

    private void disableAdministrators(List<String> removeFromOcc) {
        if(!removeFromOcc.isEmpty()) {
            logger.info("Starting disabling administrators [{}] on OCC", removeFromOcc);
            for (String idToRemove: removeFromOcc) {
                OccProfileDto occAdminProfileDto = new OccProfileDto();
                occAdminProfileDto.setxIsAdmin(false);
                logger.info("Disabling current administrator [{}] on OCC", idToRemove);
                occAdminProfileService.update(occAdminProfileDto, idToRemove);
            }
            logger.info("Finishing disabling administrators");
        }
    }

    private void updateOrganizationsList() {
        logger.info("Starting updating Organizations with Members");
        List<OccListOrganizationsDto> occList;
        Integer page = 0;
        do {
            logger.info("Requesting OCC with the values: page [{}], limit [{}]",
                    page, limit);
            occList = occOrganizationsService.listOcc(page, limit);

            sendOrganizationsListToOcc(occList);
            page++;
        } while (!occList.isEmpty());
        logger.info("Finishing Organizations Members");
    }

    private void sendOrganizationsListToOcc(List<OccListOrganizationsDto> occList) {
        for (OccListOrganizationsDto occDto : occList) {
            try {
                updateOrganizationOnOcc(occDto);
            } catch (HttpStatusCodeException e) {
                logger.error("Error sending to OCC. StatusCode [{}], Response [{}]",
                        e.getRawStatusCode(), e.getResponseBodyAsString());
            } catch (ConversionFailedException e) {
                logger.error("Error in conversion Apex to Occ object. Message [{}], Response [{}]",
                        e.getLocalizedMessage(), e.toString());
            }
        }
    }

    private void updateOrganizationOnOcc(OccListOrganizationsDto occDto) {
        String organizationId = occDto.getId();
        OccOrganizationsDto occOrganizationsDto;

        // Remove MEMBER ID from OLD ADMIN
        if (!removeFromOcc.isEmpty()) {
            occOrganizationsDto = new OccOrganizationsDto();
            occOrganizationsDto.setOp("removeContacts");
            for (String memberToRemoveId: removeFromOcc) {
                occOrganizationsDto.addMember(memberToRemoveId);
            }
            logger.info("Removing admins ID [{}] from Organization [{}]", removeFromOcc, organizationId);
            occOrganizationsService.update(occOrganizationsDto, organizationId);
        }

        // Add MEMBER ID with NEW ADMIN
        if (!addToOcc.isEmpty()) {
            occOrganizationsDto = new OccOrganizationsDto();
            for (String memberToAddId: addToOcc) {
                occOrganizationsDto.addMember(memberToAddId);
            }
            logger.info("Removing admins ID [{}] from Organization [{}]", addToOcc, organizationId);
            occOrganizationsService.update(occOrganizationsDto, organizationId);
        }

    }
}
