package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.sensedia.report.dtos.StatusReport;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;

public class StatusReportConverter {

    public StatusReport convert(IntegrationControl integrationControl) {
        StatusReport statusReport = new StatusReport();

        statusReport.setId(integrationControl.getId());
        statusReport.setEndDate(integrationControl.getEndDate());
        statusReport.setStartDate(integrationControl.getStartDate());
        statusReport.setIntegrationType(integrationControl.getIntegrationType().name());
        statusReport.setStatus(integrationControl.getStatus().name());
        statusReport.setTotalErrorRecords(integrationControl.getTotalErrorRecords());
        statusReport.setTotalRecords(integrationControl.getTotalRecords());
        statusReport.setTotalRecordsSuccessfully(integrationControl.getTotalRecordsSuccessfully());

        return statusReport;
    }
}
