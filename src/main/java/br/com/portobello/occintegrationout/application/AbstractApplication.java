package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccConverter;
import br.com.portobello.occintegrationout.application.converters.StatusReportConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.exceptions.ConversionFailedException;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpStatusCodeException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public abstract class AbstractApplication<O, A> implements IntegrationManager {

    protected final IntegrationControlRepository repository;
    protected final OccService<O> occService;
    protected final ApexService<A> apexService;
    protected final StatusReportServiceAsync statusReportService;
    protected final OccConverter<O, A> converter;
    protected Integer limit;

    private Logger logger = LoggerFactory.getLogger(AbstractApplication.class);

    public AbstractApplication(ApexService<A> apexService,
                               OccService<O> occService,
                               IntegrationControlRepository repository,
                               StatusReportServiceAsync statusReportService,
                               OccConverter<O, A> converter,
                               Integer limit) {
        this.apexService = apexService;
        this.occService = occService;
        this.repository = repository;
        this.statusReportService = statusReportService;
        this.converter = converter;
        this.limit = limit;
    }

    @Override
    public void sync() {
        logger.info("starting [{}] synchronization in OCC", getIntegrationType());
        doBeforeAll();

        IntegrationControl integrationControl = saveStartIntegration();
        logger.info("Record created in the database. Object [{}]", integrationControl);

        Instant lastExecutedDate = getLastExecutedDate();

        IntegrationMetrics metrics = new IntegrationMetrics();
        List<A> apexList;
        Integer page = 0;

        try {
            do {
                logger.info("Requesting APEX with the values: page [{}], limit [{}], lastExecutedDate [{}]",
                        page, limit, lastExecutedDate);
                apexList = apexService.list(page, limit, lastExecutedDate);

                sendListToOcc(apexList, metrics);
                page++;
            } while (!apexList.isEmpty());

            doAfterAll();

            updateEndIntegration(integrationControl, metrics, Status.SUCCESS);
        } catch (Exception e) {
            updateEndIntegration(integrationControl, metrics, Status.ERROR);
            throw e;
        } finally {
            statusReportService.send(new StatusReportConverter().convert(integrationControl));
            logger.info("finishing [{}] synchronization in OCC", getIntegrationType());
        }
    }

    private void sendListToOcc(List<A> apexList, IntegrationMetrics metrics) {
        for (A apexDto : apexList) {
            try {
                O occDto = converter.convert(apexDto);
                logger.info("Conversion from APEX to OCC successfully. Apex [{}], OCC [{}]", apexDto, occDto);

                sendToOcc(occDto, apexDto);

                metrics.registerSuccess();
            } catch (HttpStatusCodeException e) {
                logger.error("Error sending to OCC. StatusCode [{}], Response [{}]",
                        e.getRawStatusCode(), e.getResponseBodyAsString());
                metrics.registerError();
            } catch (ConversionFailedException e) {
                logger.error("Error in conversion Apex to Occ object. Message [{}], Response [{}]",
                        e.getLocalizedMessage(), e.toString());
                metrics.registerError();
            }
        }
    }

    protected void sendToOcc(O occDto, A apexDto) {
        if (occService.exists(getId(occDto))) {
            doBeforeUpdating(occDto, apexDto);
            logger.info("Calling OCC update operation.");
            occService.update(occDto, getId(occDto));
            doAfterUpdating(occDto, apexDto);
        } else {
            doBeforeCreating(occDto, apexDto);
            logger.info("Calling OCC create operation.");
            occService.create(occDto);
            doAfterCreating(occDto, apexDto);
        }
    }

    protected void doAfterCreating(O occDto, A apexDto) {};
    protected void doAfterUpdating(O occDto, A apexDto) {};
    protected void doBeforeUpdating(O occDto, A apexDto) {};
    protected void doBeforeCreating(O occDto, A apexDto) {};
    protected void doAfterAll() {};
    protected void doBeforeAll() {};

    protected abstract String getId(O occDto);

    protected abstract IntegrationType getIntegrationType();

    private Instant getLastExecutedDate() {
        Optional<IntegrationControl> optional = repository.findLastIntegrationControlWithStatusSuccess(getIntegrationType());

        if (optional.isPresent())
            return optional.get().getStartDate();

        return null;
    }

    private IntegrationControl saveStartIntegration() {
        IntegrationControl integrationControl = new IntegrationControl();

        integrationControl.setStatus(Status.PROCESSING);
        integrationControl.setIntegrationType(getIntegrationType());

        repository.save(integrationControl);

        return integrationControl;
    }

    private void updateEndIntegration(IntegrationControl integrationControl, IntegrationMetrics metrics, Status status) {
        integrationControl.setStatus(status);
        integrationControl.setEndDate(Instant.now());
        integrationControl.setTotalRecordsSuccessfully(metrics.getTotalRecordsSuccessfully());
        integrationControl.setTotalRecords(metrics.getTotalRecords());
        integrationControl.setTotalErrorRecords(metrics.getTotalErrorRecords());

        if (metrics.getTotalErrorRecords() > 0) {
            integrationControl.setStatus(Status.ERROR);
        }

        repository.save(integrationControl);
    }
}
