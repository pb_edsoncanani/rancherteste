package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexRepresentanteDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.profile.OccProfileDto;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class OccProfileConverter implements OccConverter<OccProfileDto, ApexRepresentanteDto> {
    @Override
    public OccProfileDto convert(ApexRepresentanteDto apexDto) {
        OccProfileDto occProfileDto = new OccProfileDto();

        // TODO remove email
//        occProfileDto.setEmail(apexDto.getEmail());
        occProfileDto.setEmail(getSaltString()+"@gmail.com");
        occProfileDto.setFirstName(apexDto.getPrimeiroNome());
        occProfileDto.setLastName(apexDto.getUltimoNome());
        occProfileDto.setxNomeCanal(apexDto.getNomeCanal());
        occProfileDto.setxCodigoRepresentante(apexDto.getCodRepresentante());
        occProfileDto.setxIdEbs(apexDto.getCodRepresentante());
        occProfileDto.setxIsAdmin(false);
        occProfileDto.setActive("true".equalsIgnoreCase(apexDto.getAtivo()));
        occProfileDto.setReceiveEmail("yes");
        occProfileDto.setProfileType("b2b_user");

        return occProfileDto;
    }


    public OccProfileDto convert(ApexClienteDto apexDto) {

        return null;
    }

    protected String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuU1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
}
