package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexItemPrecoDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price.OccPriceDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.price.OccPriceListDto;
import br.com.portobello.occintegrationout.exceptions.ConversionFailedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class OccPriceConverter implements OccConverter<OccPriceListDto, ApexItemPrecoDto> {

    public static final String FIX_OCC_LIST_ID = "_listPrices";

    @Override
    public OccPriceListDto convert(ApexItemPrecoDto apexDto) {
        OccPriceListDto occPriceListDto = new OccPriceListDto();

        OccPriceDto occPriceDto = new OccPriceDto();

        occPriceDto.setProductId(apexDto.getCodProduto());
        occPriceDto.setPriceListId(apexDto.getCodTabela() + FIX_OCC_LIST_ID);

        String possiblePrice = StringUtils.trimToEmpty(apexDto.getPreco());

        try {
            occPriceDto.setListPrice(Double.valueOf(possiblePrice));
        } catch (NumberFormatException e) {
            throw new ConversionFailedException("Failed to convert '{}' to Double", apexDto.getPreco());
        }

        return occPriceListDto.add(occPriceDto);
    }
}
