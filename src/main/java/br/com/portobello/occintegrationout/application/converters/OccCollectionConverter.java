package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCategoriaDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.collections.OccCollectionDto;
import org.springframework.stereotype.Component;

@Component
public class OccCollectionConverter implements OccConverter<OccCollectionDto, ApexCategoriaDto> {
    @Override
    public OccCollectionDto convert(ApexCategoriaDto apexDto) {
        OccCollectionDto occCollectionDto = new OccCollectionDto();

        occCollectionDto.setCatalogId(apexDto.getCodCanal());
        occCollectionDto.getProperties().put("id", apexDto.getCodCategoria());
        occCollectionDto.getProperties().put("displayName", apexDto.getNome());
        occCollectionDto.getProperties().put("active", apexDto.isAtivo());

        return occCollectionDto;
    }
}
