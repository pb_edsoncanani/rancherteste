package br.com.portobello.occintegrationout.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Service
public class AsyncProcessApplication {

    private final PriceListGroupApplication priceListGroupApplication;
    private final ProductApplication productApplication;
    private final SkuApplication skuApplication;
    private final PriceApplication priceApplication;
    private final UserFlowApplication userFlowApplication;
    private Logger logger = LoggerFactory.getLogger(AsyncProcessApplication.class);

    public AsyncProcessApplication(PriceListGroupApplication priceListGroupApplication,
                                   ProductApplication productApplication,
                                   SkuApplication skuApplication,
                                   PriceApplication priceApplication,
                                   UserFlowApplication userFlowApplication) {
        this.priceListGroupApplication = priceListGroupApplication;
        this.productApplication = productApplication;
        this.skuApplication = skuApplication;
        this.priceApplication = priceApplication;
        this.userFlowApplication = userFlowApplication;
    }

    @Async
    public void startCatalogSync() {
        try {
            priceListGroupApplication.sync();
            productApplication.sync();
            skuApplication.sync();
            priceApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    @Async
    public void startPriceListGroupSync() {
        try {
            priceListGroupApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    @Async
    public void startProductSync() {
        try {
            productApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    @Async
    public void startSkuSync() {
        try {
            skuApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    @Async
    public void startPriceSync() {
        try {
            priceApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    @Async
    public void startUserFlow() {
        try {
            userFlowApplication.sync();
        } catch (Exception e) {
            resolveExceptions(e);
        }
    }

    public void resolveExceptions(Exception e) {
        if (e instanceof HttpStatusCodeException) {
            HttpStatusCodeException error = (HttpStatusCodeException) e;
            logger.error("Error sending to OCC. StatusCode [{}], Response [{}], ErrorMessage [{}]", error.getRawStatusCode(),
                    error.getResponseBodyAsString(), e.getMessage());
            return;
        }

        if (e instanceof OAuth2AccessDeniedException) {
            logger.error("Failure on requesting access token.", e);
            return;
        }

        logger.error("An unexpected error has occurred", e);
    }


}
