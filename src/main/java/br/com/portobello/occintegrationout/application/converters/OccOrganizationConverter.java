package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexClienteDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccBillingAddressDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccMembersDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.organizations.OccOrganizationsDto;
import org.springframework.stereotype.Component;

@Component
public class OccOrganizationConverter implements OccConverter<OccOrganizationsDto, ApexClienteDto> {

    @Override
    public OccOrganizationsDto convert(ApexClienteDto apexDto) {

        return null;
    }
}
