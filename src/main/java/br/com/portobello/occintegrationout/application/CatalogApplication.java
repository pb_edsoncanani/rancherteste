package br.com.portobello.occintegrationout.application;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCanalDto;
import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexProdutoDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog.OccCatalogDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.product.OccProductDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.application.converters.OccCatalogConverter;
import br.com.portobello.occintegrationout.application.interfaces.IntegrationManager;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CatalogApplication extends AbstractApplication<OccCatalogDto, ApexCanalDto> implements IntegrationManager {

    private Logger logger = LoggerFactory.getLogger(CatalogApplication.class);

    public CatalogApplication(
            @Qualifier("ApexCanalService") ApexService<ApexCanalDto> apexService,
            @Qualifier("OccCatalogService") OccService<OccCatalogDto> occService,
            IntegrationControlRepository repository,
            StatusReportServiceAsync statusReportService,
            OccCatalogConverter converter,
            @Value("${app.apex.canal.list.limit}") Integer limit) {
        super(apexService, occService, repository, statusReportService, converter, limit);
    }

    @Override
    protected void doAfterCreating(OccCatalogDto occDto, ApexCanalDto apexDto) {
    }

    @Override
    protected void doAfterUpdating(OccCatalogDto occDto, ApexCanalDto apexDto) {

    }

    @Override
    protected void doBeforeUpdating(OccCatalogDto occDto, ApexCanalDto apexDto) {

    }

    @Override
    protected void doBeforeCreating(OccCatalogDto occDto, ApexCanalDto apexDto) {

    }

    @Override
    protected String getId(OccCatalogDto occDto) {
        return occDto.getCatalogId();
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.CATALOG;
    }
}
