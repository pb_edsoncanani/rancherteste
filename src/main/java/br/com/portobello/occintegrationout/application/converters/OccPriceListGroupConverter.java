package br.com.portobello.occintegrationout.application.converters;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCabecalhoPrecoDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.priceListGroup.OccPriceListGroupDto;
import br.com.portobello.occintegrationout.exceptions.ApplicationException;
import br.com.portobello.occintegrationout.exceptions.ConversionFailedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

@Component
public class OccPriceListGroupConverter implements OccConverter<OccPriceListGroupDto, ApexCabecalhoPrecoDto> {

    @Override
    public OccPriceListGroupDto convert(ApexCabecalhoPrecoDto apexDto) {
        OccPriceListGroupDto occPriceListGroupDto = new OccPriceListGroupDto();
        occPriceListGroupDto.setId(apexDto.getCodTabela());
        occPriceListGroupDto.setDisplayName(apexDto.getCodTabela());


        String apexLocale = StringUtils.substring(apexDto.getCodTabela(), 0, 2);
        if("ME".equalsIgnoreCase(apexLocale)){
            occPriceListGroupDto.setLocale("en_US");
        } else if ("MI".equalsIgnoreCase(apexLocale)){
            occPriceListGroupDto.setLocale("pt_BR");
        } else {
            throw new ConversionFailedException("Conversion failed from object [{}] ", apexDto);
        }
        return occPriceListGroupDto;
    }
}
