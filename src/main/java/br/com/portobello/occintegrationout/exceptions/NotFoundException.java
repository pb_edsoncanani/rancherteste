package br.com.portobello.occintegrationout.exceptions;

public class NotFoundException extends ApplicationException {

    public NotFoundException(Throwable cause, String message, Object... arguments) {
        super(cause, message, arguments);
    }

    public NotFoundException(String message, Object... arguments) {
        super(message, arguments);
    }
}
