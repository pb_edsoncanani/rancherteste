package br.com.portobello.occintegrationout.exceptions;

import br.com.portobello.occintegrationout.App;

public class ConversionFailedException extends ApplicationException {

    public ConversionFailedException(Throwable cause, String message, Object... arguments) {
        super(cause, message, arguments);
    }

    public ConversionFailedException(String message, Object... arguments) {
        super(message, arguments);
    }
}
