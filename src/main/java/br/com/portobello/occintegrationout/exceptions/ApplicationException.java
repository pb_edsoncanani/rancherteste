package br.com.portobello.occintegrationout.exceptions;

import org.slf4j.helpers.MessageFormatter;

public abstract class ApplicationException extends RuntimeException {

    public ApplicationException(Throwable cause, String message, Object... arguments) {
        super(MessageFormatter.arrayFormat(message, arguments).getMessage(), cause);
    }

    public ApplicationException(String message, Object... arguments) {
        this(null, message, arguments);
    }

}
