package br.com.portobello.occintegrationout;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCanalDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.catalog.OccCatalogDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.application.CatalogApplication;
import br.com.portobello.occintegrationout.application.converters.OccCatalogConverter;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CatalogApplicationTest extends AbstractTest {

    @Mock
    protected ApexService<ApexCanalDto> apexService;
    @Mock
    protected OccService<OccCatalogDto> occService;
    @Captor
    protected ArgumentCaptor<OccCatalogDto> occDtoCaptor;
    private CatalogApplication application;

    @BeforeEach
    void setup() {
        application = new CatalogApplication(apexService, occService, repository,
                statusReportService, new OccCatalogConverter(), 2);
    }

    @Test
    @DisplayName("Como Portobello quero atualizar um catalogo no OCC com sucesso")
    void sendExistingCatalogSuccessfully() {
        List<ApexCanalDto> apexList = createApexMock(1);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(true);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-canal-1");
        verify(occService, times(1)).update(occDtoCaptor.capture(), any());
        verify(occService, never()).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero criar um catalogo no OCC com sucesso")
    void sendCatalogThatDoesNotExistSuccessfully() {
        List<ApexCanalDto> apexList = createApexMock(1);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(false);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-canal-1");
        verify(occService, times(1)).create(occDtoCaptor.capture());
        verify(occService, never()).update(any(), any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero atualizar um catalogo no OCC a partir de uma data no banco de dados")
    void sendCatalogFromDate() {
        List<ApexCanalDto> apexList = createApexMock(1);
        IntegrationControl integrationControl = createIntegrationControlMock();

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.of(integrationControl));
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(false);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(1)).list(0, 2, INTEGRATION_CONTROL_START_DATE);

        verify(occService, times(1)).exists("cod-canal-1");
        verify(occService, times(1)).create(occDtoCaptor.capture());
        verify(occService, never()).update(any(), any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero atualizar um catalogo no OCC com paginação")
    void sendCatalogWithPaginatedData() {
        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(createApexMock(2),
                createApexMock(1, 3), new ArrayList<>());
        when(occService.exists(any())).thenReturn(true, false, true);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(1)).list(0, 2, null);
        verify(apexService, times(1)).list(1, 2, null);
        verify(apexService, times(1)).list(2, 2, null);

        verify(occService, times(1)).exists("cod-canal-1");
        verify(occService, times(1)).exists("cod-canal-2");
        verify(occService, times(1)).exists("cod-canal-3");

        verify(occService, times(2)).update(occDtoCaptor.capture(), any());
        verify(occService, times(1)).create(occDtoCaptor.capture());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 3, 3, 0);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());

        assertThat(occDtoCaptor.getAllValues()).hasSize(3);
    }

    @Test
    @DisplayName("Como Portobello quero testar uma falha na comunicação com APEX")
    void testApexCallFailure() {
        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenThrow(
                new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

        assertThrows(HttpServerErrorException.class, () -> {
            application.sync();
        });

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(1)).list(any(), any(), any());

        verify(occService, never()).exists(any());
        verify(occService, never()).update(any(), any());
        verify(occService, never()).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 0, 0, 0, Status.ERROR);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero testar uma falha na comunicação com OCC")
    void testOccCallFailure() {
        List<ApexCanalDto> apexList = createApexMock(2);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(true, false);
        doThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST)).when(occService).update(any(), any());

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.CATALOG);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-canal-1");
        verify(occService, times(1)).exists("cod-canal-2");

        verify(occService, times(1)).update(any(), any());
        verify(occService, times(1)).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 2, 1, 1, Status.ERROR);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    private void validateOcc(OccCatalogDto occDto) {
        validateOcc(occDto, 1);
    }

    private void validateOcc(OccCatalogDto occDto, int autoId) {
        assertThat(occDto.getCatalogId()).isEqualTo("cod-canal-" + autoId);
        assertThat(occDto.getDisplayName()).isEqualTo("canal " + autoId);
    }

    private List<ApexCanalDto> createApexMock(int amount) {
        return createApexMock(amount, 1);
    }

    private List<ApexCanalDto> createApexMock(int amount, int startId) {
        List<ApexCanalDto> apexList = new ArrayList<>();

        for (int i = startId; i < amount + startId; i++) {
            ApexCanalDto apexDto = new ApexCanalDto();

            apexDto.setCodCanal("cod-canal-" + i);
            apexDto.setCanal("canal " + i);

            apexList.add(apexDto);
        }

        return apexList;
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.CATALOG;
    }
}
