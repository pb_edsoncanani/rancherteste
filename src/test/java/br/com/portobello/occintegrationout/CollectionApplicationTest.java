package br.com.portobello.occintegrationout;

import br.com.portobello.occintegrationout.adapters.apex.dtos.ApexCategoriaDto;
import br.com.portobello.occintegrationout.adapters.apex.interfaces.ApexService;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.dtos.collections.OccCollectionDto;
import br.com.portobello.occintegrationout.adapters.sensedia.occ.interfaces.OccService;
import br.com.portobello.occintegrationout.adapters.sensedia.report.dtos.StatusReport;
import br.com.portobello.occintegrationout.application.CollectionApplication;
import br.com.portobello.occintegrationout.application.converters.OccCollectionConverter;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class CollectionApplicationTest extends AbstractTest {

    private CollectionApplication application;

    @Mock
    private ApexService<ApexCategoriaDto> apexService;

    @Mock
    private OccService<OccCollectionDto> occService;

    @Captor
    private ArgumentCaptor<OccCollectionDto> occDtoCaptor;

    @Captor
    private ArgumentCaptor<StatusReport> statusReportCaptor;

    @BeforeEach
    void setup() {
        application = new CollectionApplication(apexService, occService, repository,
                statusReportService, new OccCollectionConverter(), 2);
    }

    @Test
    @DisplayName("Como Portobello quero atualizar uma coleção no OCC com sucesso")
    void sendExistingCollectionSuccessfully() {
        List<ApexCategoriaDto> apexList = createApexMock(1);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(true);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-categoria-1");
        verify(occService, times(1)).update(occDtoCaptor.capture(), any());
        verify(occService, never()).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero criar uma coleção no OCC com sucesso")
    void sendCollectionThatDoesNotExistSuccessfully() {
        List<ApexCategoriaDto> apexList = createApexMock(1);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(false);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-categoria-1");
        verify(occService, times(1)).create(occDtoCaptor.capture());
        verify(occService, never()).update(any(), any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero atualizar uma coleção no OCC a partir de uma data no banco de dados")
    void sendCollectionFromDate() {
        List<ApexCategoriaDto> apexList = createApexMock(1);
        IntegrationControl integrationControl = createIntegrationControlMock();

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.of(integrationControl));
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(false);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(1)).list(0, 2, INTEGRATION_CONTROL_START_DATE);

        verify(occService, times(1)).exists("cod-categoria-1");
        verify(occService, times(1)).create(occDtoCaptor.capture());
        verify(occService, never()).update(any(), any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue());
        validateOcc(occDtoCaptor.getValue());
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero atualizar uma coleção no OCC com paginação")
    void sendCollectionWithPaginatedData() {
        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(createApexMock(2),
                createApexMock(1, 3), new ArrayList<>());
        when(occService.exists(any())).thenReturn(true, false, true);

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(1)).list(0, 2, null);
        verify(apexService, times(1)).list(1, 2, null);
        verify(apexService, times(1)).list(2, 2, null);

        verify(occService, times(1)).exists("cod-categoria-1");
        verify(occService, times(1)).exists("cod-categoria-2");
        verify(occService, times(1)).exists("cod-categoria-3");

        verify(occService, times(2)).update(occDtoCaptor.capture(), any());
        verify(occService, times(1)).create(occDtoCaptor.capture());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 3, 3, 0);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());

        assertThat(occDtoCaptor.getAllValues()).hasSize(3);
    }

    @Test
    @DisplayName("Como Portobello quero testar uma falha na comunicação com APEX")
    void testApexCallFailure() {
        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenThrow(
                new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

        assertThrows(HttpServerErrorException.class, () -> {
            application.sync();
        });

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(1)).list(any(), any(), any());

        verify(occService, never()).exists(any());
        verify(occService, never()).update(any(), any());
        verify(occService, never()).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 0, 0, 0, Status.ERROR);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    @Test
    @DisplayName("Como Portobello quero testar uma falha na comunicação com OCC")
    void testOccCallFailure() {
        List<ApexCategoriaDto> apexList = createApexMock(2);

        when(repository.findLastIntegrationControlWithStatusSuccess(any())).thenReturn(Optional.empty());
        when(apexService.list(any(), any(), any())).thenReturn(apexList, new ArrayList<>());
        when(occService.exists(any())).thenReturn(true, false);
        doThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST)).when(occService).update(any(), any());

        application.sync();

        verify(repository, times(2)).save(integrationControlCaptor.capture());
        verify(repository, times(1)).findLastIntegrationControlWithStatusSuccess(IntegrationType.COLLECTION);

        verify(apexService, times(2)).list(any(), any(), any());

        verify(occService, times(1)).exists("cod-categoria-1");
        verify(occService, times(1)).exists("cod-categoria-2");

        verify(occService, times(1)).update(any(), any());
        verify(occService, times(1)).create(any());

        verify(statusReportService, times(1)).send(statusReportCaptor.capture());

        validateDatabase(integrationControlCaptor.getValue(), 2, 1, 1, Status.ERROR);
        validateStatusReport(statusReportCaptor.getValue(), integrationControlCaptor.getValue());
    }

    private void validateOcc(OccCollectionDto occCollectionDto) {
        validateOcc(occCollectionDto, 1);
    }

    private void validateOcc(OccCollectionDto occCollectionDto, int autoId) {
        assertThat(occCollectionDto.getCatalogId()).isEqualTo("cod-canal-" + autoId);
        assertThat(occCollectionDto.getProperties().get("id")).isEqualTo("cod-categoria-" + autoId);
        assertThat(occCollectionDto.getProperties().get("displayName")).isEqualTo("nome " + autoId);
        assertThat(occCollectionDto.getProperties().getValueBoolean("active")).isTrue();
    }

    private List<ApexCategoriaDto> createApexMock(int amount) {
        return createApexMock(amount, 1);
    }

    private List<ApexCategoriaDto> createApexMock(int amount, int startId) {
        List<ApexCategoriaDto> apexList = new ArrayList<>();

        for (int i = startId; i < amount + startId; i++) {
            ApexCategoriaDto apexDto = new ApexCategoriaDto();

            apexDto.setAtivo(true);
            apexDto.setCodCanal("cod-canal-" + i);
            apexDto.setCodCategoria("cod-categoria-" + i);
            apexDto.setNome("nome " + i);

            apexList.add(apexDto);
        }

        return apexList;
    }

    @Override
    protected IntegrationType getIntegrationType() {
        return IntegrationType.COLLECTION;
    }
}
