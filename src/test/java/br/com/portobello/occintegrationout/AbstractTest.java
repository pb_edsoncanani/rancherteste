package br.com.portobello.occintegrationout;

import br.com.portobello.occintegrationout.adapters.sensedia.report.StatusReportServiceAsync;
import br.com.portobello.occintegrationout.adapters.sensedia.report.dtos.StatusReport;
import br.com.portobello.occintegrationout.infra.repository.IntegrationControlRepository;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationControl;
import br.com.portobello.occintegrationout.infra.repository.schemas.IntegrationType;
import br.com.portobello.occintegrationout.infra.repository.schemas.Status;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractTest {

    static final Instant INTEGRATION_CONTROL_START_DATE = Instant.parse("2020-02-12T13:00:00Z");

    @Mock
    protected IntegrationControlRepository repository;

    @Mock
    protected StatusReportServiceAsync statusReportService;

    @Captor
    protected ArgumentCaptor<IntegrationControl> integrationControlCaptor;

    @Captor
    protected ArgumentCaptor<StatusReport> statusReportCaptor;

    void validateDatabase(IntegrationControl integrationControl) {
        validateDatabase(integrationControl, 1, 1, 0);
    }

    void validateDatabase(IntegrationControl integrationControl, int total, int totalSuccess, int totalError) {
        validateDatabase(integrationControl, total, totalSuccess, totalError, Status.SUCCESS);
    }

    void validateDatabase(IntegrationControl integrationControl, int total, int totalSuccess,
                          int totalError, Status status) {
        assertThat(integrationControl.getIntegrationType()).isEqualTo(getIntegrationType());
        assertThat(integrationControl.getStatus()).isEqualTo(status);
        assertThat(integrationControl.getStartDate()).isNotNull();
        assertThat(integrationControl.getEndDate()).isNotNull();
        assertThat(integrationControl.getTotalErrorRecords()).isEqualTo(totalError);
        assertThat(integrationControl.getTotalRecordsSuccessfully()).isEqualTo(totalSuccess);
        assertThat(integrationControl.getTotalRecords()).isEqualTo(total);
    }

    void validateStatusReport(StatusReport statusReport, IntegrationControl integrationControl) {
        assertThat(statusReport.getId()).isEqualTo(integrationControl.getId());
        assertThat(statusReport.getIntegrationType()).isEqualTo(integrationControl.getIntegrationType().name());
        assertThat(statusReport.getStatus()).isEqualTo(integrationControl.getStatus().name());
        assertThat(statusReport.getStartDate()).isEqualTo(integrationControl.getStartDate());
        assertThat(statusReport.getEndDate()).isEqualTo(integrationControl.getEndDate());
        assertThat(statusReport.getTotalRecordsSuccessfully()).isEqualTo(integrationControl.getTotalRecordsSuccessfully());
        assertThat(statusReport.getTotalRecords()).isEqualTo(integrationControl.getTotalRecords());
        assertThat(statusReport.getTotalErrorRecords()).isEqualTo(integrationControl.getTotalErrorRecords());
    }

    IntegrationControl createIntegrationControlMock() {
        IntegrationControl integrationControl = new IntegrationControl();

        integrationControl.setId("5e43f7ffaa175606dfa420e6");
        integrationControl.setStartDate(INTEGRATION_CONTROL_START_DATE);
        integrationControl.setIntegrationType(getIntegrationType());
        integrationControl.setStatus(Status.SUCCESS);
        integrationControl.setEndDate(Instant.parse("2020-02-12T13:05:00Z"));
        integrationControl.setTotalErrorRecords(0);
        integrationControl.setTotalRecords(1);
        integrationControl.setTotalRecordsSuccessfully(1);

        return integrationControl;
    }

    protected abstract IntegrationType getIntegrationType();
}
